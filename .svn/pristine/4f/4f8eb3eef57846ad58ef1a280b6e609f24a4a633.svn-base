/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

.item-relations-row{}

#item-details-info-tab-1 h2,
#item-details-info-tab-1 h3 {
  display: none;
}

#item-details-info-tab-1 .item-relations-cell {
  list-style: none;
}

.facets-item-cell-list-left {
  @extend .col-xs-3;
}

.facets-item-cell-list-right {
  @extend .col-xs-9;

  .item-views-price {
    text-align:left;
  }
}

.facets-item-cell-list .item-details-option-color-tiles-container {
  @extend .color-picker-sm;
}

.facets-item-cell-list .item-details-option-color-label {
  display:none;
}

.item-relations-row{
  border-bottom: 1px solid #000;
  padding: 15px 0;
  margin: 0;
  @media(max-width: 667px){
    padding: 7px 0;
  }
}

.item-details-content-related-items{
  .facets-item-cell-list-quick-view-wrapper
  {
    display: none;
  }
  .item-cell:hover .item-cell-quick-view-wrapper {
    display: none;
  }

  .facets-item-cell-list-details{
    width: 40%;
    padding: 1% 15px;
    @media only screen and (max-width: 667px){
      width: 75%;
    }

  }
  .facets-item-cell-list-item{
    padding: 10px 0;
    text-align: center;
    @media only screen and (max-width: 667px){
      width: 25%;
      border-left: 3px solid #0054A1;
    }
  }
  .facets-item-cell-list-price-container{
    width: 43%;

    @media only screen and (max-width: 667px){
      width: 100%;
      border-left: 3px solid #3f97da;
      border-top: 1px solid #f6f6f6;
      margin: 2px 0 0;

    }
    .facets-item-cell-list-price{
      width: 40%;
      line-height: 20px;
      @media only screen and (max-width: 667px){
        width: 35%;
        padding: 1.5% 5px;
      }
      .item-views-price-exact{
        padding: 0;
        display: block;
        @media only screen and (max-width: 667px){
          padding: 10% 0;
        }
      }
      .facets-item-cell-list-min-qry{
        float: left;
        width: 35%;
        text-align: right;
        color: #000;
      }
      .facets-item-cell-list-min-qry-price{
        float: left;
        width: 55%;
        text-align: left;
        margin-left: 10%;
      }
    }
    .facets-item-cell-list-add-to-cart{

      .facets-item-cell-list-qty{
        width: 20%;
        padding: 0;
        @media only screen and (max-width: 667px){
          padding: 0;
        }
      }
      .facets-item-cell-list-stock{
        width: 20%;
        padding: 0;
        img{
          width: 70%;
        }
        @media only screen and (max-width: 667px){
          padding: 4% 0;
        }
      }
      .facets-item-cell-list-addtocart-button{
        width: 19%;
        padding: 0;
        .add-to-cart-item-list{
          background: none;
          height: auto;
          padding: 0;
          position: relative;
        }
      }

      .facets-item-cell-list-qty,
      .facets-item-cell-list-stock,
      .facets-item-cell-list-addtocart-button{
        @media only screen and (max-width: 667px){
          width: 21.6%;
          img{
            width: 35%;
          }
        }
      }

    }
  }
}


aside.item-relations-related {
    margin-bottom: 50px;
}




/*cart detail*/
.cart-detailed-left {
  .item-relations-correlated-row {
    .facets-item-cell-list-details {
      @media only screen and (min-width: 667px){
        width: 25%;
      }
    }
    .facets-item-cell-list-price-container {
      @media only screen and (min-width: 667px){
        width: 58%;
        .facets-item-cell-list-price {
            width: 35%;
        }
        .facets-item-cell-list-add-to-cart .facets-item-cell-list-qty {
            width: 38%;
            padding: 0;
        }
        .facets-item-cell-list-add-to-cart .item-views-cell-actionable-options {
            padding: 0;
            display: block;
        }
      }
    }
  }
  .facets-facet-browse-details-column {
    @media only screen and (min-width: 667px){
      width: 26%;
    }
  }
  .facets-facet-browse-price-content-column {
      > div.facets-facet-browse-price-column {
          width: 30%;
      }
      > div.facets-facet-browse-qty-column {
          width: 40%;
      }
      > div.facets-facet-browse-addtocart-column {
          width: 30%;
      }
      @media only screen and (min-width: 667px){
        width: 57%;
        > div.facets-facet-browse-price-column {
            width: 33%;
        }
        > div.facets-facet-browse-qty-column {
            width: 39%;
        }
        > div.facets-facet-browse-addtocart-column {
            width: 28%;
            padding:12px 6px;
        }
      }
  }
  .facets-item-cell-list {
    display: flex;
    flex-wrap: wrap;
    margin:0 0 10px;
    @media only screen and (min-width: 667px){
      padding: 0;
      border-bottom:1px solid #ccc
    }
    @media only screen and (max-width: 667px){
      .facets-item-cell-list-price {
        width: 30%
      }
      .facets-item-cell-list-add-to-cart {
        float: left;
        width: 70%;
        .facets-item-cell-list-qty {
          width: 60%
        }
        .item-views-cell-actionable-options {
          float: left;
          width: 40%;
          padding:25px 0;
        }
      }
    }
  }
}






  /*later*/
  .product-list-details-later-list-items {
    .backbone-collection-view-row {
      margin: 0;
    }
    article.facets-item-cell-list {
        margin: 0;
        padding: 15px 0;
        border-bottom: 1px solid #ccc;
    }
  }



  .facets-item-cell-list.product-list-details-later-macro-selectable-actionable {
    .facets-item-cell-list-details {
      width: 40%;
    }
    .facets-item-cell-list-price-container {
        width: 43.333%;
    }
    .facets-item-cell-list-price {
        width: 40%;
    }
    .facets-item-cell-list-qty {
        width: 20%;
    }
    .facets-item-cell-list-stock {
        width: 19%;
    }
    .facets-item-cell-list-addtocart-button {
        width: 21%;
        padding: 0;
    }
    .facets-item-cell-list-addtocart-button > div > div {
        width: 100%;
    }
    .facets-item-cell-list-addtocart-button > div > div button {
        max-width: 100%;
        display: block;
        position: relative;
        margin: 0 0 10px;
        width: 100%;
    }
    .facets-item-cell-list-addtocart-button > div > div button.add-to-cart-item-list {
        background: transparent;
    }
    .facets-item-cell-list-addtocart-button > div > div button.add-to-cart-item-list img {
        height: 44px;
        width: auto;
    }
    .facets-item-cell-list-item {
        padding: 10px;
    }
    .facets-item-cell-stock-img,
    .header-mini-cart-menu-cart-icon {
        margin-top: -10px;
        max-width: 50px;
    }

  }


@media (max-width: 667px) {

.product-list-details-later-list-items article.facets-item-cell-list {
    padding: 0;
    border-bottom: 1px solid #000;
    margin-bottom: 10px;
}

.facets-item-cell-list.product-list-details-later-macro-selectable-actionable .facets-item-cell-list-item {
    width: 41%;
    border-left: 4px solid #0054A1;
}

.facets-item-cell-list.product-list-details-later-macro-selectable-actionable .facets-item-cell-list-details {
    width: 59%;
}

.facets-item-cell-list.product-list-details-later-macro-selectable-actionable .facets-item-cell-list-price-container {
    width: 100%;
    border-left: 4px solid #3f97da;
    border-top: 1px solid #ccc;
    margin-top: 1px;
}

}









@media (min-width: 667px) {


  /*mini cart*/

  .header-mini-cart {
    .facets-facet-browse-items-column,
    .facets-facet-browse-price-content-column,
    .facets-item-cell-list-item,
    .facets-facet-browse-columns .facets-facet-browse-price-content-column .facets-facet-browse-price-column,
    .facets-facet-browse-columns .facets-facet-browse-price-content-column .facets-facet-browse-qty-column {
      width: 50%;
    }
    .facets-item-cell-list-item a {
        padding: 10px 0 0;
        display: block;
        word-break: break-all;
    }
    .facets-item-cell-list-price {
        color: #cb2222;
        padding: 10px 0 0;
        font-size: 12px;
        font-weight: 400;
    }
    .facets-item-cell-list-qty {
        padding: 10px 15px 0;
        text-align: center;
    }
    li.header-mini-cart-item-cell {
        margin-bottom: 5px;
        padding-bottom: 10px;
        border-bottom: 1px solid #ccc;
    }

  }

}


  /*order wizard*/
  .order-wizard-cartitems-module-accordion-container {
    .facets-facet-browse-columns {
      .facets-facet-browse-price-content-column {
        .facets-facet-browse-amount-column {
          width: 40%;
          float: left;
          text-align: center;
          padding: 12px 5px;
        }
      }
    }
  }
  .order-wizard-cartitems-module-table {
    .facets-item-cell-list {
      padding: 10px 15px;
      border-bottom: 1px solid #ccc;
      a.item-views-cell-navigable-product-title-anchor {
        font-weight: 400;
        font-size: 13px;
        word-break: break-all;
      }
      .facets-item-cell-list-item {
        padding: 0px 10px;
      }
    }
    .facets-item-cell-list-details {
      width: 40%;
    }
    .facets-item-cell-list-price-container {
      padding:0;
      width: 18%;
    }
    .facets-item-cell-list-price {
      width: 100%
    }
    .facets-item-cell-list-amount-container {
      float: left;
      width: 17%;
      text-align: center;
    }
    .facets-item-cell-list-qty-container {
      float: left;
      width: 8%;
      .facets-item-cell-list-qty {
        width: 100%;
        padding: 4px 0;
        .item-views-cell-navigable-item-quantity-value {
          text-align: center;
        }
      }
    }
    @media (max-width: 667px) {
        .facets-item-cell-list {
            display: flex;
            margin: 0 0 15px;
            flex-wrap: wrap;
            border-bottom: 1px solid;
            padding: 0;
        }

        .facets-item-cell-list-item {
            width: 40%;
            border-left: 4px solid #0054A1;
        }

        .facets-item-cell-list-details {
            width: 60%;
        }


        .facets-item-cell-list-price-container {
            width: 35%;
            border-left: 4px solid #3f97da;
            margin-top: 2px;
        }

        .facets-item-cell-list-qty-container {
            width: 22%;
            float: left;
            padding: 15px;
        }

        .facets-item-cell-list-amount-container {
            float: left;
            width: 43%;
            padding: 10px;
        }

    }
  }

  /*order wizard column right*/

  .order-wizard-step-content-secondary {

    @media (max-width: 667px) {
        .facets-item-cell-list {
            display: flex;
            margin: 0 0 15px;
            flex-wrap: wrap;
            border-bottom: 1px solid;
            padding: 0;
        }

        .facets-item-cell-list-item {
            width: 40%;
            border-left: 4px solid #0054A1;
        }

        .facets-item-cell-list-details {
            width: 60%;
        }


        .facets-item-cell-list-price-container {
            width: 35%;
            border-left: 4px solid #3f97da;
            margin-top: 2px;
        }

        .facets-item-cell-list-qty-container {
            width: 22%;
            float: left;
            padding: 15px;
        }

        .facets-item-cell-list-amount-container {
            float: left;
            width: 43%;
            padding: 10px;
        }

    }

  @media (min-width: 667px) {
    .facets-facet-browse-columns .facets-facet-browse-details-column, 
    .order-wizard-cartitems-module-table .facets-item-cell-list-details {
      display: none;
    }
    .facets-facet-browse-columns {
      .facets-facet-browse-items-column {
        width: 40%;
      }
      .facets-facet-browse-price-content-column {
        width: 60%;
      }
    }
    .order-wizard-cartitems-module-table {
      .facets-item-cell-list .facets-item-cell-list-item {
        width: 40%;
        padding: 5px 15px;
      }
      .facets-item-cell-list-price-container {
        width: 25%;
        .item-views-cell-navigable-item-unit-price-label {
          display: none;
        }
        .facets-item-cell-list-price {
          width: 100%;
        }
      }
      .facets-item-cell-list-qty-container {
        width: 12%;
        padding: 1px 0;
      }
      .facets-item-cell-list-amount-container {
        width: 23%;
        text-align: center;
        .item-views-cell-navigable-item-amount-label {
          display: none;
        }
      }
    }
  }

}







  /*my account*/
.order-history-fulfillment-body,
.order-history-shipping-group-accordion-container {

@media (max-width: 667px) {
  .facets-facet-browse-columns .facets-facet-browse-items-column {
      width: 100%;
      border: none;
  }

  .facets-facet-browse-columns .facets-facet-browse-price-content-column .facets-facet-browse-qty-column {
      width: 40%;
      line-height: 22px;
  }

  .facets-facet-browse-columns .facets-facet-browse-price-content-column .facets-facet-browse-addtocart-column {
      width: 60%;
      line-height: 22px;
  }

  .facets-item-cell-list {
      margin: 0 0 10px;
  }
}

  @media (min-width: 667px) {
    .facets-facet-browse-columns {
      .facets-facet-browse-items-column {
        width: 30%;
      }
      .facets-facet-browse-price-content-column {
        width: 70%;
        .facets-facet-browse-qty-column {
            width: 50%;
        }
        .facets-facet-browse-addtocart-column {
            width: 50%;
            padding: 12px 5px;
        }
      }
    }
  }

}


  .order-history-fulfillment-items-table,
  .order-history-shipping-group-pending-table {


    @media (max-width: 667px) {
      .facets-item-cell-list-item {
          border-left: 4px solid #0054A1;
          width: 40%;
          padding: 15px;
      }

      .facets-item-cell-list-details {
          width: 60%;
      }

      .facets-item-cell-list-price-container {
          width: 100%;
          border-left: 4px solid #3f97da;
          margin-top: 2px;
          border-top: 1px solid #ccc;
          padding: 15px 0;
      }

      .facets-item-cell-list-price {
          float: left;
          width: 40%;
      }

      form.facets-item-cell-list-add-to-cart {
          float: left;
          width: 60%;
      }

      .facets-item-cell-list-qty {
          width: 100%;
      }

      .item-views-cell-actionable-options {
          clear: both;
      }

    }



  @media (min-width: 667px) {

    .facets-item-cell-list {
        margin: 10px 0;
        padding-bottom: 10px;
        border-bottom: 1px solid #ccc;
    }
    .facets-item-cell-list-item {
        width: 30%;
    }

    .facets-item-cell-list-price-container {
        padding: 0;
        width: 70%;
    }
    .facets-item-cell-list-price, 
    .facets-item-cell-list-details {
        display: none;
    }

    form.facets-item-cell-list-add-to-cart {
        width: 100%;
        float: left;
        .facets-item-cell-list-qty {
            width: 50%;
            float: left;
        }
        .item-views-cell-actionable-options {
            width: 50%;
            float: left;
        }

    }

  }

}












  .reorder-items-list-hedaer {
      @media (max-width: 667px) {

        .facets-facet-browse-stock-column {
            display: none;
        }

        .facets-facet-browse-columns .facets-facet-browse-price-content-column .facets-facet-browse-qty-column {
            width: 35%;
        }

      }
  }



  .reorder-items-list-reorder-items-table {

    @media (max-width: 667px) {
      .facets-item-cell-list {
          margin: 0 0 10px;
      }

      .facets-item-cell-list-item {
          width: 43%;
          border-left: 4px solid #0054A1;
      }

      .facets-item-cell-list-details {
          width: 57%;
      }

      .facets-item-cell-list-price-container {
          border-left: 4px solid #3f97da;
          margin-top: 2px;
          padding: 15px 0;
          width: 100%;
      }

      .facets-item-cell-list-price {
          width: 33%;
      }

      form.facets-item-cell-list-add-to-cart {
          float: left;
          width: 67%;
      }

      .facets-item-cell-list-qty {
          padding: 0;
          width: 50%;
          float: left;
      }

      .item-views-cell-actionable-options {
          float: left;
          width: 50%;
          padding: 20px 5px;
      }

    }




  @media (min-width: 667px) {
    
      .facets-item-cell-list {
          margin: 0;
          padding: 10px 0;
          border-bottom: 1px solid #ccc;
      }

      .facets-item-cell-list-details {
          width: 40%;
      }

      .facets-item-cell-list-price-container {
          width: 43.3333%;
      }

      .facets-item-cell-list-price {
          width: 40%;
          float: left;
      }

      form.facets-item-cell-list-add-to-cart {
          float: left;
          width: 60%;
      }

      .facets-item-cell-list-qty {
          width: 30%;
          padding: 0;
      }
    }





  }




.reorder-items-list {
  .facets-facet-browse-stock-column {
      display: none;
  }

  .facets-facet-browse-addtocart-column {
      width: 40% !important;
  }
}













@media (min-width: 667px) {


  /*my account bill of material*/

  .product-list-details-list-items {

    .facets-item-cell-list {
        margin: 0;
        padding: 10px 0;
        border-bottom: 1px solid #ccc;
    }

    .product-list-display-full-select {
        float: left;
        width: 1%;
        padding: 6px 0;
    }
    .facets-item-cell-list-item {
        width:15%;
    }
    .facets-item-cell-list-item a {
        font-weight: 400;
        font-size: 13px;
        word-break: break-all;
    }

    .facets-item-cell-list-details {
        width: 41%;
    }

    .facets-item-cell-list-price-container {
        width: 10%;
        padding: 0;
    }

    .facets-item-cell-list-price {
        width: 100%;
    }

    span.item-views-price-lead {
        font-size: 14px;
        font-weight: 400;
    }

    .product-list-display-full-extras {
        float: left;
        width: 21%;
        text-align: center;
        padding: 0 12px;
    }

    .product-list-display-full-actions {
        width: 8%;
        float: left;
        display: block;
        min-width: 100px;
        padding: 0;
    }


    .facets-facet-browse-columns .facets-facet-browse-price-content-column {
      
      .facets-facet-browse-price-column {
          width: 25%;
      }
      .facets-facet-browse-qty-column {
          width: 48%;
      }
      .facets-facet-browse-addtocart-column {
          width: 27%;
          padding: 12px 5px;
      }

    }



  }


    .item-views-price-lead{
      font-size: 14px !important;
    }

}
/*end query*/


.facets-item-cell-list {
  border-bottom: 1px solid #000;
  margin-bottom: 10px;
  display: flex;
  flex-wrap: wrap;
  @media(min-width: 667px){
    /*padding: 0 15px;*/
    border-bottom:1px solid #ccc
  }
  .facets-item-cell-list-item {
    font-size:12px;
    line-height: 16px;
  }
}
.item-relations-row {
  border-bottom:none;
}


div#shopping-cart > div[data-type="alert-placeholder"] {
    max-width: 1140px;
    margin: 0 auto;
    > div {
      width: 100%;
      display: block;
    }
}


.facets-facet-browse-facets .facets-faceted-navigation-facets-clear {
  position: relative;
}
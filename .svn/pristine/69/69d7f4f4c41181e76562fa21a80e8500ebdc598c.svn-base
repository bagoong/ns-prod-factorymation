/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

.facets-item-cell-list {
	@extend .row;
	@extend .item-cell;
}

.facets-item-cell-list-quick-view-wrapper {
	@extend .item-cell-quick-view-wrapper;
	top: 0;
	> a{
		width: 50px;
		height: 50px;
		display: inline-block;
	}
}

.facets-item-cell-list-quick-view-link {
	@extend .item-cell-quick-view-link;
}

.facets-item-cell-list-left {
	@extend .col-xs-3;
}

.facets-item-cell-list-right {
	@extend .col-xs-9;

	.item-views-price {
		text-align:left;
	}
}

.facets-item-cell-list-image-wrapper {
	position:relative;
	text-align:center;
}

.facets-item-cell-list-anchor {}

.facets-item-cell-list-quick-view-icon {}

.facets-item-cell-list-name{}

.facets-item-cell-list-add-to-cart {}

.facets-item-cell-list-add-to-cart-itemid {}

.facets-item-cell-list-add-to-cart-quantity {}

.facets-item-cell-list-add-to-cart-button {}

.facets-item-cell-list-stock {
	text-transform: uppercase;
    font-size: 11px;
    font-weight: bold;
    text-align: center;
    line-height: 11px;
    .label-in-stock{
    	color:  #74C13D;
    }
    .label-out-of-stock{
    	color: red;
    }
    .icon-in-stock{
		@extend .fa;
		color:  #74C13D;
		display: block;
    	&:before{
    		content: '\f00c';
    		font-size: 18px;
    	}
    }
    .icon-out-of-stock{
		@extend .icon-remove;
		@extend .fa-2x;
    }
}

.facets-item-cell-list-details {
	text-align:center;
}

.facets-item-cell-list-image {
	width:100%;
	height:auto;
}

.facets-item-cell-list-title {
	@extend .item-cell-title;
	text-align:left;
}

.facets-item-cell-list-price {
	@extend .small-price;
	margin: 0;
}

.facets-item-cell-list-rating {
	text-align:center;
	display: inline-block;
}

.facets-item-cell-list .item-details-option-color-tiles-container {
	@extend .color-picker-sm;
}

.facets-item-cell-list .item-details-option-color-label {
	display:none;
}

.facets-item-cell-list {
	margin-bottom: 0;
	.global-views-star-rating-area {
		margin-bottom:$sc-small-margin;
	}
}


.facets-items-collection-view-row{
	border-bottom: 1px solid #000;
	padding: 15px 0;
	margin: 0;
	@media only screen and (min-width: 667px){
		border-bottom:none;
		padding: 0
	}

	.facets-items-collection-view-cell-span12{
		padding: 0;
		.facets-item-cell-list{
			.facets-item-cell-list-price-container {
				.facets-item-cell-list-price{
					.item-views-price-lead, .item-views-price-exact{
						font-size: 14px !important;
						font-weight: normal;
					}
				}


			}
			margin: 0;
		}
	}
}
.facets-item-cell-list-item,
.facets-facet-browse-items-column{
	@extend .col-xs-2;
	word-break: break-all;
}
.facets-item-cell-list-details,
.facets-facet-browse-details-column{
	@extend .col-xs-6;
	.facets-item-cell-list-description{
		text-align: left;
		font-size: 12px;
		line-height: 16px;
	}
}
.facets-facet-browse-details-column,
.facets-facet-browse-items-column,
.facets-facet-browse-price-column,
.facets-facet-browse-qty-column{
	padding: 12px 5px;
}
.facets-facet-browse-stock-column,
.facets-facet-browse-details-column,
.facets-facet-browse-items-column,
.facets-facet-browse-price-column,
.facets-facet-browse-qty-column{
	border-right: 1px solid white;
}
.facets-facet-browse-addtocart-column,
.facets-facet-browse-stock-column{
	padding: 6px 0;
}
.facets-item-cell-list-price-container,
.facets-facet-browse-price-content-column{
	@extend .col-xs-4;
	padding: 0;
}
.facets-item-cell-list-price-container {
	padding:10px 0;
}
.facets-item-cell-list-addtocart-button,
.facets-item-cell-list-qty,
.facets-item-cell-list-stock{
	@extend .col-xs-3;
}
.facets-item-cell-list-stock{
	padding:0;
}
.facets-item-cell-list-price,
.facets-facet-browse-price-column,
.facets-facet-browse-qty-column,
.facets-facet-browse-stock-column,
.facets-facet-browse-addtocart-column{
	@extend .col-xs-3;
	text-align: center;
}
.facets-item-cell-list-price{
	padding: 0 5px;
}
.facets-item-cell-list-min-qry-container{
	padding:0;
	font-size: 11px;
    float: left;
    width: 100%;
	.facets-item-cell-list-min-qry,
	.facets-item-cell-list-min-qry-price{
		@extend .col-xs-6;
		padding: 0;
	}
	.facets-item-cell-list-min-qry{
		text-align: left;
	}
	.facets-item-cell-list-min-qry-price{
		text-align: right;
	}
}
.facets-item-cell-list-qty{
	input{
		border: 1px solid #dfdfdf;
		padding: 3px;
		width: 100%;
		text-align: center;
	    color: black;
   		font-weight: 400;
	}
	.item-views-option-dropdown-color, .item-views-option-text{
		margin-top: 15px;
		.item-views-option-dropdown-color-label, .item-views-option-text-title{
			font-size: $sc-smallest-font-size + 1;
			color: #000000;
			font-weight: 400;
		}
		select{
			padding: 0 $sc-base-padding;
			width: 100%;
			height: 23px;
			background: none;
			color: #000000;
			border: 1px solid #000000;
		}
		input {
			min-width: 50px;
   			border: 1px solid #000;
		}
	}
}
.facets-facet-browse-columns{
	width: 100%;
	display: inline-block;
	background: #0054A1;
	color: white;
	font-size: 11px;
	text-transform: uppercase;
	height: 36px;

	@media only screen and (max-width: 667px){
		height: auto;
	}

	.facets-facet-browse-items-column{
		@media only screen and (max-width: 667px){
			width: 25%;
		}
		@media(max-width: 480px){
			width: 44%;
			text-align: center;
		}
	}

	.facets-facet-browse-details-column{
		width: 40%;
		@media only screen and (max-width: 667px){
			width: 75%;
			border: 0;
		}
		@media(max-width: 480px){
			width: 56%;
			text-align: center;
		}
	}
	.facets-facet-browse-price-content-column{
		width: 43%;
		@media only screen and (max-width: 667px){
			width: 100%;
			border-top: 1px solid #fff;
			background: #3f97da;
			div{
				padding: 12.5px 15px;
				@media(max-width: 555px) and (min-width: 320px){
					height: 50px;
				}
			}
		}
		.facets-facet-browse-price-column{
			width: 40%;
			@media only screen and (max-width: 667px){
				width: 35%;
			}
		}

		.facets-facet-browse-qty-column,
		.facets-facet-browse-stock-column,
		.facets-facet-browse-addtocart-column{
			width: 20%;
		    padding: 10px;
			@media only screen and (max-width: 667px){
				width: 21.6%;
			}
		}

		.facets-facet-browse-addtocart-column{
			@media(max-width: 362px){
				padding: 0;
			}
		}
		.facets-item-cell-list-stock img{
			width: 55%;
			@media(max-width: 320px){
				width: 60%;
			}
		}

	}
}

.facets-facet-browse-items, .item-relations-related-row, .item-relations-correlated-row{
	.item-relations-row {
		padding: 0;
	}
	.facets-item-cell-list-details{
		width: 40%;
		padding: 10px;
		@media only screen and (max-width: 667px){
			width: 70%;
		}
		@media only screen and (max-width: 480px){
			width: 60%;
		}
	}
	.facets-item-cell-list-item{
		padding: 10px;
		a.facets-item-cell-list-name{
			display: block;
			text-align: center;
		}
		@media only screen and (max-width: 667px){
			width: 30%;
			border-left: 3px solid #0054A1;
			padding: 3.8% 10px;
		}
		@media(max-width: 480px){
			width: 40%;
		}

	}
	.facets-item-cell-list-price-container{
		width: 43%;

		@media only screen and (max-width: 667px){
			width: 100%;
		    border-left: 3px solid #3f97da;
		    border-top: 1px solid #f6f6f6;
		    margin: 2px 0 0;

		}
		.facets-item-cell-list-price{
			width: 40%;
			line-height: 20px;
			@media only screen and (max-width: 667px){
				width: 35%;
				padding: 1.5% 5px;
			}
			.item-views-price-exact{
				padding: 0;
				display: block;
				@media only screen and (max-width: 667px){
					/*padding: 17% 0;*/
				}
				span{
					color: $sc-color-red;
				}
			}
			.facets-item-cell-list-min-qry{
				float: left;
			    width: 35%;
			    text-align: right;
			    color: #000;
			}
			.facets-item-cell-list-min-qry-price{
				float: left;
				width: 55%;
			    text-align: left;
			    margin-left: 10%;
			    color: $sc-color-red;
			}
		}
		.facets-item-cell-list-add-to-cart{

			.facets-item-cell-list-qty{
				width: 20%;
    			padding: 0;
    			@media only screen and (max-width: 667px){
    				padding: 5px 0;
    			}
			}
			.facets-item-cell-list-stock{
				width: 23%;
				padding: 0;
				img{
					width: 70%;
					@media only screen and (max-width: 768px){
    					width: 80%;
    				}
				}

				@media only screen and (max-width: 667px){
    				padding: 0;
    			}
			}
			.facets-item-cell-list-addtocart-button{
				width: 17%;
				padding: 0;
				.add-to-cart-item-list{
					background: none;
				    height: auto;
				    padding: 0;
				    position: relative;
				}
			}

			.facets-item-cell-list-qty,
			.facets-item-cell-list-stock,
			.facets-item-cell-list-addtocart-button{
				@media only screen and (max-width: 667px){
					width: 21.6%;
					img{
						width: 45%;
					}
				}
				@media only screen and (max-width: 375px){
					img{
						width: 70%;
					}
				}
			}

		}
	}
}

.facets-item-cell-list-no-price-message{
	color: $sc-color-red;
	font-size: 14px;
	font-weight: 400;
	margin-bottom: $sc-base-margin;
}

.facets-item-cell-list-LTL{
	margin-top: 5px;
	text-align: left;
	.LTL-truck{
		max-width: 40px;
		margin: 0 3px;
	}
	img{
		position: relative;
	    top: 6px;
	    margin-right: 5px;
	    /*width: 15%;*/
	    max-width: 32px;
	    @media(max-width: 667px){
	    	top: 3px;
	    	width: 32px;
	    }
	}
	@media only screen and (max-width: 320px){
		font-size: 11px;
		img{
			width: 15%;
		}
	}
}

.add-to-cart-item-list{
  outline: none;
	@extend .nav-search-button-submit;
	.header-mini-cart-menu-cart-icon{
		color: white;
		line-height: 37px;
		&.icon-white{
			background: transparent;
		}
	}
}

.facets-facet-browse-items-column, .facets-facet-browse-details-column {
  text-align: center;
}

ul.recently-viewed-row-cell li {
  width: 100%;
}

.item-relations-related, .item-relations-correlated{
  .facets-item-cell-list{
	margin-left: 0%;
	margin-right: 0;
  }
  .facets-facet-browse-addtocart-column{
	padding: 12px 0;
  }
}
@media(min-width: 768px) {
  .facets-item-cell-stock-img,
  .header-mini-cart-menu-cart-icon {
	margin-top: -10px;
	max-width: 50px;
  }
  .add-to-cart-item-list {
	max-width: 80%;
  }
}

.facets-item-cell-list-description b {
	color:inherit;
	text-align:left;
}
.item-views-price-lead {
	font-weight:400 !important;
}

.item-views-cell-actionable-stock-notice {
	margin-top: 10px;
	text-align: left;
	color: $sc-color-red;
}

.item-views-cell-actionable-backorder-stock-notice {
	margin-top: 10px;
	text-align: left;
	color: $sc-color-red;
}

.item-views-cell-actionable-selected-options-cell .item-views-selected-option span {
    font-size: 12px;
}



.cart-detailed-left {
	.facets-item-cell-list-LTL {
	    margin-top:0;
	}
	.facets-facet-browse-price-content-column .facets-facet-browse-addtocart-column {
	    line-height: 25px;
	}
}


.facets-item-cell-list {
	.facets-item-cell-list-quick-view-wrapper {
	    width: 40px;
	    left: auto;
	    top: 7px;
	    display: none !important;
	}

	.facets-item-cell-list-quick-view-wrapper>a {
	    width: 40px;
	    height: 40px;
	}

	.facets-item-cell-list-description {
	    padding-right: 20px;
	}
	.facets-item-cell-list-details {
		&:hover .facets-item-cell-list-quick-view-wrapper {
	    	display: block !important;
	    }
	}
}




.facets-facet-browse-items .facets-item-cell-list-details, .item-relations-related-row .facets-item-cell-list-details, .item-relations-correlated-row .facets-item-cell-list-details {
	@media(max-width: 667px){
		padding:10px 0 10px 10px;
		width: 58%;
	}
}

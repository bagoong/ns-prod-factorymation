define('Site.Shopping.Configuration', [
    'SC.Shopping.Configuration',
    'Site.Global.Configuration',

    'facets_faceted_navigation_item_range.tpl',
    'facets_faceted_navigation_item_color.tpl',
    'facets_faceted_navigation_item.tpl',
    'facets_item_cell_list.tpl',

    'underscore',
    'Utils'
], function SiteShoppingConfiguration(
    ShoppingConfiguration,
    GlobalConfiguration,

    facetsFacetedNavigationItemRangeTemplate,
    facetsFacetedNavigationItemColorTemplate,
    facetsItemListShowSelector,
    facets_item_cell_list_tpl,
    _

) {
    'use strict';

    ShoppingConfiguration.lightColors.push('White');

    var SiteApplicationConfiguration = {
        itemDetails: [{
            name: _('Specifications').translate(),
            contentFromKey: 'storedetaileddescription',
            opened: true,
            itemprop: 'description'
        },
            {
                name: _('Media').translate(),
                contentFromKey: 'custitem_documents',
                opened: false,
                itemprop: 'description'
            }],
        facets: [{
            id: 'custitem_height1',
            name: _('Height').translate(),
            priority: 40,
            behavior: 'range',
            macro: 'facetRange',
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: false,
            titleSeparator: ', ',
            notprice: true,
            collapsed: false,
            parser: function parser(value) {
                value = Number(value);
                var inch2dec= value.toFixed(2);
                var mm = value * 25.4;
                var mm2dec = mm.toFixed(2);

                return inch2dec + ' ('+ mm2dec +'mm)';
            }
        },{
            id: 'custitem_width1',
            name: _('Width').translate(),
            priority: 39,
            behavior: 'range',
            macro: 'facetRange',
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: false,
            collapsed: false,
            titleSeparator: ', ',
            notprice: true,
            parser: function parser(value) {
                value = Number(value);
                var inch2dec= value.toFixed(2);
                var mm = value * 25.4;
                var mm2dec = mm.toFixed(2);

                return inch2dec + ' ('+ mm2dec +'mm)';
            }
        },{
            id: 'custitem_depth1',
            name: _('Depth').translate(),
            priority: 38,
            behavior: 'range',
            macro: 'facetRange',
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: false,
            titleSeparator: ', ',
            notprice: true,
            collapsed: false,
            parser: function parser(value) {
                value = Number(value);
                var inch2dec= value.toFixed(2);
                var mm = value * 25.4;
                var mm2dec = mm.toFixed(2);

                return inch2dec + ' ('+ mm2dec +'mm)';
            }
        },{
            id: 'Category_',
            name: _('Category').translate(),
            priority: 37,
            behavior: 'multi',
            macro: 'facetRange',
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Item_Type',
            name: _('Item Type').translate(),
            priority: 36,
            behavior: 'multi',
            macro: 'facetRange',
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Primary',
            name: _('Primary').translate(),
            priority: 35,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Secondary',
            name: _('Secondary').translate(),
            priority: 34,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'HP',
            name: _('HP').translate(),
            priority: 33,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Input_Power',
            name: _('Input Power').translate(),
            priority: 32,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Output_Power',
            name: _('Output Power').translate(),
            priority: 31,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Amps',
            name: _('Amps').translate(),
            priority: 30,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Nominal_Amps',
            name: _('Nominal Amps').translate(),
            priority: 29,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Poles',
            name: _('Poles').translate(),
            priority: 28,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Light_Type',
            name: _('Light Type').translate(),
            priority: 27,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Voltage',
            name: _('Voltage').translate(),
            priority: 26,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Connection',
            name: _('Connection').translate(),
            priority: 25,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Phase',
            name: _('Phase').translate(),
            priority: 24,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Interrupt_Rating',
            name: _('Interrupt Rating').translate(),
            priority: 23,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Input_Configuration',
            name: _('Input Configuration').translate(),
            priority: 22,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Coil_Voltage',
            name: _('Coil Voltage').translate(),
            priority: 21,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'O-L_Range',
            name: _('O/L Range').translate(),
            priority: 20,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'RPM',
            name: _('RPM').translate(),
            priority: 19,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Size',
            name: _('Size').translate(),
            priority: 18,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Output_Configuration',
            name: _('Output Configuration').translate(),
            priority: 17,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Sensing_Range',
            name: _('Sensing Range').translate(),
            priority: 16,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Capacity',
            name: _('Capacity').translate(),
            priority: 15,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Wire_Range',
            name: _('Wire Range').translate(),
            priority: 14,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Housing',
            name: _('Housing').translate(),
            priority: 13,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Frame',
            name: _('Frame').translate(),
            priority: 12,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Mount',
            name: _('Mount').translate(),
            priority: 11,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Environmental_Rating',
            name: _('Environmental Range').translate(),
            priority: 10,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Color',
            name: _('Color').translate(),
            priority: 9,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Material',
            name: _('Material').translate(),
            priority: 8,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Manufacturer',
            name: _('Manufacturer').translate(),
            priority: 7,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'Availability',
            name: _('Availability').translate(),
            priority: 6,
            behavior: 'multi',
            macro: 'facetRange',
            collapsed: true,
            template: facetsItemListShowSelector,
            uncollapsible: false,
            max: 5
        },{
            id: 'onlinecustomerprice',
            name: _('Price').translate(),
            priority: 1,
            behavior: 'range',
            macro: 'facetRange',
            collapsed: true,
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: true,
            titleToken: 'Price $(1) - $(0)',
            titleSeparator: ', ',
            parser: function parser(value) {
                return _.formatCurrency(value);
            }
        }, {
            // this is for the case that onlinecustomerprice is not available in the account
            id: 'pricelevel5',
            name: _('Price').translate(),
            priority: 1,
            behavior: 'range',
            macro: 'facetRange',
            template: facetsFacetedNavigationItemRangeTemplate,
            uncollapsible: true,
            titleToken: 'Price $(1) - $(0)',
            titleSeparator: ', ',
            collapsed: true,
            parser: function parser(value) {
                return _.formatCurrency(value);
            }
        }]

        ,itemsDisplayOptions: [
            {id: 'list', name: _('List').translate(), template: facets_item_cell_list_tpl, columns: 1, icon: 'icon-display-list', isDefault: true}
        ]

        ,addToCartFromFacetsView: true

        ,sortOptions: [
            {id: 'custitemweb_sort_order:desc', name: _('Sort by relevance').translate(), isDefault: true}
            ,   {id: 'itemid:asc', name: _('Sort by name, ascending').translate()}
            ,   {id: 'itemid:desc', name: _('Sort by name, descending').translate()}
            ,	{id: 'onlinecustomerprice:asc', name: _('Sort by price, low to high').translate()}
            ,	{id: 'onlinecustomerprice:desc', name: _('Sort by price, high to low ').translate()}
        ]


    };

    var extraModulesConfig = GlobalConfiguration.extraModulesConfig;
    delete GlobalConfiguration.extraModulesConfig;

    ShoppingConfiguration.facetsSeoLimits.numberOfFacetsGroups = 0;
    ShoppingConfiguration.modulesConfig = ShoppingConfiguration.modulesConfig || {};

    _.extend(ShoppingConfiguration, GlobalConfiguration, SiteApplicationConfiguration);
    _.extend(ShoppingConfiguration.modulesConfig, extraModulesConfig);



    return {
        mountToApp: function mountToApp(application) {
            _.extend(application.Configuration, ShoppingConfiguration);
        }
    };
});

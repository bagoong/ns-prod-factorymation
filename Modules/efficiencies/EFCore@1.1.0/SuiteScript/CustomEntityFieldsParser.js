define('CustomEntityFieldsParser', [
    'underscore'
], function CustomEntityFieldsParser(
    _
) {
    'use strict';
    return function customEntityFieldsParser(customEntityFieldsResponse) {
        return _.object(
            _.map(customEntityFieldsResponse, function mapResponse(field) {
                return [field.name, field.value];
            })
        );
    };
});
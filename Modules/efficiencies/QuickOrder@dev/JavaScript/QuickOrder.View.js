/*
 © 2015 NetSuite Inc.
 User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
 provided, however, if you are an authorized user with a NetSuite account or log-in, you
 may use this code subject to the terms that govern your access and use.
 */

define('QuickOrder.View', [
    'Backbone',
    'ItemDetails.Collection',
    'ItemDetails.Model',
    'QuickOrder.Line.View',
    'Backbone.CollectionView',
    'Backbone.CompositeView',
    'QuickOrder.Collection',
    'quickorder.tpl',
    'LiveOrder.Model',
    'GlobalViews.Message.View',
    'ErrorManagement',
    'jQuery',
    'underscore',
    'LiveOrder.Model.MultiLine'
], function QuickOrderView(Backbone,
                           ItemCollection,
                           ItemModel,
                           QuickOrderLineView,
                           CollectionView,
                           CompositeView,
                           Collection,
                           quickorder_tpl,
                           LiveOrderModel,
                           GlobalViewsMessageView,
                           ErrorManagement,
                           jQuery,
                           _) {
    'use strict';
    return Backbone.View.extend({

        template: quickorder_tpl,

        events: {
            'click button[data-action="multiadd"]': 'addToCart',
            'click button[data-action="addEmptyLine"]': 'addLine',
            'click a[data-action="removeLine"]': 'removeLine',
            'focus input[data-field="itemid"]': 'lineIndex',
            'change input[data-field="itemqty"]': 'updateQty'
        },
        index: 1,

        initialize: function initialize() {
            var self = this;
            // Get configuration for initial Line numbers
            var configInitialLines = 1//this.options.application.getConfig('quickOrder.initialLines');
            // New collection - to store each line (index, query, search results)
            this.collection = new Backbone.Collection();
            // Add empty item model x initial lines in config
            _(configInitialLines).times(function times() {
                self.addLine(true);
            });
            // child views
            CompositeView.add(this);
            this.on('afterCompositeViewRender', this.configureTypeahead, this);
        },

        configureTypeahead: function () {
            var self = this;
            for (var i = 1; i < this.index; i++) {
                this.$el.find('[data-field="itemid"][data-index=' + i + ']').typeahead({
                        minLength: 3
                    }, this.getTypeAheadConfiguration()
                );
                this.$el.find('[data-field="itemid"][data-index=' + i + ']').on('typeahead:selected', _.bind(self.resultSelected, self));
            }
        },

        childViews: {
            'QuickView.CollectionView': function QuickViewCollectionView() {
                return new CollectionView({
                    collection: this.collection,
                    childView: QuickOrderLineView
                });
            }
        },

        /*enterKey: function enterkey(e) {
         var self = this;
         var enter = true;
         if (event.keyCode === 13)  {
         self.itemQuery(e, enter);
         }
         },*/

        addLine: function addLine() {
            var item;
            var line;
            var self = this;
            // For each new line in collection add index, item model and search results
            this.collection.add(new Backbone.Model({
                internalid: this.index,
                item: new ItemModel(),
                suggestedResults: new Collection(),
                referenceLine: _.uniqueId('quickorder_'),
                addedToCart: false,
                cartMessage: '',
                cartCode: ''
            }));

            // Create quantity attribute and give initial value
            line = this.collection.findWhere({internalid: this.index});
            item = line.get('item');
            item.set('quantity', 1);

            // Increment index number
            this.index++;
            this.render();
        },

        addToCart: function addToCart() {
            // event.preventDefault();
            var self = this;
            var cart = LiveOrderModel.getInstance();
            // get each item
            var itempluck = this.collection.map(function map(line) {
                var item = line.get('item');

                // Set unique line id
                item.set('addedToCart', line.get('addedToCart'));
                item.set('referenceLine', line.get('referenceLine'));
                return line.get('item');
            });
            // filter out empty line items (e.g. new line) & lines already added to cart
            var filtered = _.reject(itempluck, function filtered(num) {
                return !_.has(num, 'id') || num.get('addedToCart') === true || num.get('_isPurchasable') === false;
            });

            cart.addMultipleItems(filtered).done(function done(response) {
                // map through returned properties and assign against appropriate item model
                _.map(response.multilineResponse, function map(x, y) {
                    var line = self.collection.findWhere({'referenceLine': y});
                    if (typeof x === 'string' && x.indexOf('item') !== -1) {
                        line.set('addedToCart', true);
                    } else {
                        line.set('addedToCart', false);
                        line.set('cartMessage', x.message);
                        line.set('cartCode', x.code);
                    }
                });
                self.render();
            });
        },

        removeLine: function removeLine(e) {
            var self = this;
            var $button = jQuery(e.currentTarget);
            // line internal id, stored as data attr
            var index = $button.data('index');
            // Find correct internalid in model array
            var linewithid = this.collection.findWhere({internalid: index});
            this.collection.remove(linewithid);
            this.render();
            var count = $("div[data-view='QuickView.CollectionView'] > div").length;
            if (count == 0){
                setTimeout(function(){
                    self.addLine()
                },250)

            }

        },

        updateQty: function updateQty(e) {
            var $element = jQuery(e.target);
            var self = this;
            var index = $element.data('index');
            var quantity = parseInt(jQuery('[data-field="itemqty"][data-index=' + index + ']').val(), 10);
            var line = this.collection.findWhere({internalid: index});
            var item = line.get('item');
            var minQuantity = item.get('_minimumQuantity');

            // Set quantity to minimum if current value is lower
            if (quantity >= minQuantity) {
                item.set('quantity', quantity);
            } else {
                item.set('quantity', quantity);
            }
            self.render();
            $element.focus();
        },

        resultSelected: function itemQuery(e, itemSelected) {
            var $select = jQuery(e.target);
            var index = $select.data('index');
            var quantity = parseInt(jQuery('[data-field="itemqty"][data-index=' + index + ']').val(), 10);
            // Create object key/value before using in findWHere
            // Get object first based on query string then find using internalid
            var line = this.collection.findWhere({internalid: index});
            //var itemSelected = line.get('suggestedResults').findWhere({internalid: $selectedinternalid});
            var item;
            var minQuantity;
            line.set('item', itemSelected);
            line.set('query', itemSelected.get('_name'));
            item = line.get('item');
            minQuantity = item.get('_minimumQuantity');

            // Set quantity to minimum if current value is lower
            if (quantity >= minQuantity) {
                item.set('quantity', quantity);
            } else {
                item.set('quantity', minQuantity);
            }
        },

        getTypeAheadConfiguration: function () {
            var self = this;
            return {
                source: _.bind(self.loadSuggestionItemsSource, self)
                , displayKey: _.bind(self.getSelectedItemDisplayText, self)
            };
        },

        loadSuggestionItemsSource: function (query, callback) {
            var self = this;
            var index = this.lineFoucusIndex
            // Get query based on index due to button
            var query = query.trim();
            var querylength = query.length;
            // Get previous search term, only needed for focusout
            var quantity = parseInt(jQuery('[data-field="itemqty"][data-index=' + index + ']').val(), 10);
            // find internal id in collection and
            var line = this.collection.findWhere({internalid: index});
            var item;
            var minQuantity;

            line.set('suggestedResults', new Collection());
            line.set('query', query);
            // reset ItemModel
            line.set('item', new ItemModel());
            // Add the Qty back - removed by new ItemModel
            item = line.get('item');
            item.set('quantity', quantity);

            // if query empty or same as previous do not fetch
            if (query !== '') {
                // use fetch method for ajax call, data object for params to send
                line.get('suggestedResults').fetch({
                    data: {
                        keyword: query
                    }
                }).done(function done() {
                    // if collection empty show error use error handler maybe 'showerror'
                    if (line.get('suggestedResults').length === 1) {
                        // add
                        line.set('item', line.get('suggestedResults').at(0));
                        item = line.get('item');
                        minQuantity = item.get('_minimumQuantity');

                        // Set quantity to minimum is current value is lower
                        if (quantity >= minQuantity) {
                            item.set('quantity', quantity);
                        } else {
                            item.set('quantity', minQuantity);
                        }
                    } else if (line.get('suggestedResults').length === 0) {
                        // no results
                    }
                    callback(line.get('suggestedResults').models);
                });
            }
        },

        getSelectedItemDisplayText: function (itemModel) {
            return itemModel.get('_name');
        },

        lineIndex: function (e) {
            var $element = jQuery(e.target);
            var index = $element.data('index');
            this.lineFoucusIndex = index;
        }
    });
});

/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ItemDetails
define(
	'ItemDetails.View.Extend'
,	[
		'ItemDetails.View'
    ,	'Profile.Model'
    ,	'ItemRelations.Related.View'
		,	'LiveOrder.Model'
	]
,	function (
		ItemDetailsView
	,	ProfileModel
	,	ItemRelationsRelatedView
		,	LiveOrderModel
	)
{
	'use strict';

	_.extend(ItemDetailsView.prototype,{
		//@method getContext
		//@return {ItemDetails.View.Context}

		dataViews: _.extend( ItemDetailsView.prototype.childViews, {
			'Related.Items': function ()
			{
				var view =  new ItemRelationsRelatedView({ itemsIds: this.model.get('internalid'), application: this.application });

				view.on('afterViewRender', function () {
					if( !this.collection.length )
						jQuery('#item-details-content-container-1, .item-details-tab-title[data-index="1"]').remove()
				});

				return view;
			}
		}),

		getContext: function ()
		{
			var model = this.model
				,	thumbnail = model.get('_images', true)[0] || model.get('_thumbnail')
				,	selected_options = model.getSelectedOptions()

				,	quantity = model.get('quantity')
				,	min_quantity = model.get('_minimumQuantity', true)
				,	min_disabled = false;

			var cart = 	LiveOrderModel.getInstance();

			var cartItems = cart.get('lines');
			var found = false;
			for(var i = 0; i < cartItems.length && this.model.isReadyForCart(); i++){
				if(this.model.id == cartItems.models[i].get("item").id) {
					var cartItemOptions = cartItems.models[i].attributes.options;
					if(cartItemOptions) {
						var sameOptions = true;
						for (var j = 0; j < selected_options.length; j++) {
							sameOptions = sameOptions && (selected_options[j].internalid == cartItemOptions[j].value);
						}
						if (sameOptions) {
							found = true;
							break;
						}
					} else {
						found = true;
						break;
					}
				}
			}
			if(found){
				min_quantity = 1;
			}

			if (model.get('quantity') <= model.get('_minimumQuantity', true))
			{
				// TODO: resolve error with circular dependency.
				if (require('LiveOrder.Model').loadCart().state() === 'resolved')
				{
					// TODO: resolve error with circular dependency.
					var itemCart = SC.Utils.findItemInCart(model, require('LiveOrder.Model').getInstance())
						,	total = itemCart && itemCart.get('quantity') || 0;

					if ((total + model.get('quantity')) <= model.get('_minimumQuantity', true))
					{
						min_disabled = true;
					}
				}
				else
				{
					min_disabled = false;
				}
			}

			var details_object = model.get('_priceDetails') || {}
				, hasPriceSchedule = false
				, priceSchedule = [];

			if (details_object.priceschedule && details_object.priceschedule.length) {
				hasPriceSchedule = true;

				var price, min;

				for (var i = 0; i < details_object.priceschedule.length; i++) {
					price = details_object.priceschedule[i];
					min = parseInt(price.minimumquantity, 10);
					min = min == 0 ? 1 : min;
					priceSchedule.push({
						price: price.price_formatted,
						minimunQty: min
					});
				}
			}
			if(model.get('custitem_item_right_banner') && model.get('custitem_item_right_banner')!=""){
				jQuery("#banner-right-pdp").hide();
			}
			//@class ItemDetails.View.Context
			return {
				//@property {ItemDetails.Model} model
				model: model
				//@property {Boolean} isPriceEnabled
				,	isPriceEnabled: !ProfileModel.getInstance().hidePrices()
				//@property {Array<ItemDetailsField>} details
				,	details: this.details
				//@property {Boolean} showDetails
				,	showDetails: this.details.length > 0
				//@property {Boolean} isItemProperlyConfigured
				,	isItemProperlyConfigured: model.isProperlyConfigured()
				//@property {Boolean} showQuantity
				,	showQuantity: model.get('_itemType') === 'GiftCert'
				//@property {Boolean} showRequiredReference
				,	showRequiredReference: model.get('_itemType') === 'GiftCert'
				//@property {Boolean} showSelectOptionifOutOfStock
				,	showSelectOptionMessage : !model.isSelectionComplete() && model.get('_itemType') !== 'GiftCert'
				//@property {Boolean} showMinimumQuantity
				,	showMinimumQuantity: !! min_quantity && min_quantity > 1
				//@property {Boolean} isReadyForCart
				,	isReadyForCart: model.isReadyForCart()
				//@property {Boolean} showReviews
				,	showReviews: this.reviews_enabled
				//@property {String} itemPropSku
				,	itemPropSku: '<span itemprop="sku">' + model.get('_sku', true) + '</span>'
				//@property {String} item_url
				,	item_url : model.get('_url') + model.getQueryString()
				//@property {String} thumbnailUrl
				,	thumbnailUrl : this.options.application.resizeImage(thumbnail.url, 'main')
				//@property {String} thumbnailAlt
				,	thumbnailAlt : thumbnail.altimagetext
				//@property {String} sku
				,	sku : model.get('_sku', true)
				//@property {Boolean} isMinQuantityOne
				,	isMinQuantityOne : model.get('_minimumQuantity', true) === 1
				//@property {Number} minQuantity
				,	minQuantity : min_quantity
				//@property {Number} quantity
				,	quantity : quantity
				//@property {Boolean} isMinusButtonDisabled
				,	isMinusButtonDisabled: min_disabled || model.get('quantity') === 1
				//@property {Boolean} hasCartItem
				,	hasCartItem : !!model.cartItemId
				//@property {Array} selectedOptions
				,	selectedOptions: selected_options
				//@property {Boolean} hasSelectedOptions
				,	hasSelectedOptions: !!selected_options.length
				//@property {Boolean} hasAvailableOptions
				,	hasAvailableOptions: !!model.getPosibleOptions().length
				//@property {Boolean} isReadyForWishList
				,	isReadyForWishList: model.isReadyForWishList()
				,	storeDescription: model.get('storedescription')
				,	stockMessage: model.get('outofstockmessage')
				,	inStockImgSource: _.getAbsoluteUrl('img/in-stock.png')
        ,	inStock: model.get('isinstock')
        ,	outOfStockImgSource: _.getAbsoluteUrl('img/out-of-stock.png')
        ,	dropShipImgSource: _.getAbsoluteUrl('img/drop-ship-item.png')
        ,	dropShipItem: model.get('isdropshipitem')
		,	isBuiltToOrderImgSource: _.getAbsoluteUrl('img/BuiltToOrder_icon.png')
		,	isBuiltToOrder: model.get('custitem98')
        ,	specialOrderImgSource: _.getAbsoluteUrl('img/lead-time-item.png')
        ,	specialOrderItem: model.get('isspecialorderitem')
		,   itemrightbanner: model.get('custitem_item_right_banner') // right banner
				,	hasPriceSchedule: hasPriceSchedule
				,	priceSchedule: priceSchedule
				,	dontShowPrice: model.get('dontshowprice')
				,	noPriceMessage: model.get('nopricemessage')
				,   LTLMessage: model.get('_LTLmessage')
				,    LTLimgSource: _.getAbsoluteUrl('img/LTL.PNG')
				,    mustLTL: model.get('_mustLTL')

			};
		}
	})
});

//@class ItemDetails.View.Initialize.Parameters
//@property {ItemDetails.Model} model
//@property {String} baseUrl
//@property {ApplicationSkeleton} application

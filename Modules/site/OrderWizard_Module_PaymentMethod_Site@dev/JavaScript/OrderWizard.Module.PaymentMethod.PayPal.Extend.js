/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module OrderWizard @class OrderWizard.Module.PaymentMethod.Paypal @extends OrderWizard.Module.PaymentMethod
define(
	'OrderWizard.Module.PaymentMethod.PayPal.Extend'
,	[
		'OrderWizard.Module.PaymentMethod.PayPal'
	,	'Session'
	,	'Backbone'
	,	'underscore'
	,   'jQuery'
	]
,	function (
		OrderWizardModulePaymentMethodPayPal
	,	OrderWizardModulePaymentMethod
	,	Session
	,	Backbone
	,	_
	, 	jQuery
	)
{
	'use strict';

     _.extend(OrderWizardModulePaymentMethodPayPal.prototype, {

	submit: function ()
		{
			var ponumber = self.$('[name=purchase-order-number]').val() || '' ;   
            var options = this.model.get('options') || {};

            this.model.set('options', _.extend(options, {
                custbody_ns_cc_po: ponumber
            }));
		}

	,	getContext: function ()
		{

			var option = this.model.get("options");
            var purchaseorder = option.custbody_ns_cc_po;

			return {
					//@property {Boolean} isPaypalComplete
					isPaypalComplete: !!this.model.get('isPaypalComplete')
					//@property {String} paypalImageUrl
				,	paypalImageUrl: this.wizard.application.getConfig('paypal_logo', 'img/paypal.png')

				,   purchaseNumber: purchaseorder

			};
		}

	});
});
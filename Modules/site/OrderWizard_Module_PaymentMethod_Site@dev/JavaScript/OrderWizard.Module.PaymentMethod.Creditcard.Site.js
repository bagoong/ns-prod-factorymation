define('OrderWizard.Module.PaymentMethod.Creditcard.Site', [
    'OrderWizard.Module.PaymentMethod.Creditcard',
    'underscore'
],
function OrderWizardModulePaymentMethodCreditcardSite(
    OrderWizardModulePaymentMethodCreditcard,
    _
) {
    'use strict';

    OrderWizardModulePaymentMethodCreditcard.prototype.installPlugin('postContext', {


        name: 'OrderWizardModulePaymentMethodCreditcardPostContext',
        priority: 10,
        execute: function execute(context, view) {

            var options = view.model.get('options');

            if (options) {
                context.purchaseNumber = options.custbody_ns_cc_po;
            } else {
                context.costcenter = view.model.get('costcenter') || '';
                context.custmemo = view.model.get('custmemo') || '';
            }
        }
    });

    _.extend(OrderWizardModulePaymentMethodCreditcard.prototype, {
        submit: _.wrap(OrderWizardModulePaymentMethodCreditcard.prototype.submit, function submit(fn) {

            var ponumber = self.$('[name=purchase-order-number]').val() || '' ;
            var options = this.model.get('options') || {};

            this.model.set('options', _.extend(options, {
                custbody_ns_cc_po: ponumber
            }));

            return fn.apply(this, _.toArray(arguments).slice(1));
        })

    ,   getContext: function ()
        {
            var showForm = this.creditcards.length === 0 && (!this.creditcard || this.creditcard && this.creditcard.isNew());
            var option = this.model.get("options");
            var purchaseorder = option.custbody_ns_cc_po;

            if (!showForm)
            {
                // Skip_Login fix, because when starting the wizard, the user is guest, he has no credit cards so we show the CreditCard.Form and we set it on this.creditCardView
                // But then when the user logs in, this.creditCardView still exists and the submit thinks it's a new credit card
                this.creditcardView = null;
            }

            //@class OrderWizard.Module.PaymentMethod.Creditcard.Context
            return {
                //@property {Boolean} showForm
                showForm: showForm
                //@property {Boolean} showList
            ,   showList: !!(this.creditcards.length >= 1 && !this.creditcard)
                //@property {Boolean} showSelectedCreditCard
            ,   showSelectedCreditCard: !!(this.creditcard && !this.creditcard.isNew())
                //@property {Boolean} showTitle
            ,   showTitle: !!this.getTitle()
                //@property {String} title
            ,   title: this.getTitle()
                //@property {CreditCard.Model} selectedCreditCard
            ,   selectedCreditCard: this.creditcard
                //@property {CreditCard.Collection} creditCards
            ,   creditCards: this.creditcards

            ,   purchaseNumber: purchaseorder

            };
        }

    });
});

define('OrderWizard.Module.PaymentMethod.Site', [
    'OrderWizard.Module.PaymentMethod.Creditcard.Site',
    'OrderWizard.Module.PaymentMethod.PayPal.Extend'

], function OrderWizardModulePaymentMethodSite() {
    'use strict';

    // Main entry point
    return {};
});

{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="order-wizard-paymentmethod-paypal-module-row">
	<div class="order-wizard-paymentmethod-paypal-module-column-left">
		<img class="order-wizard-paymentmethod-paypal-module-paypal-logo" src="{{paypalImageUrl}}" alt="PayPal">
	</div>
	<div class="order-wizard-paymentmethod-paypal-module-column-right">

		{{#if isPaypalComplete}}
			<p>
				{{translate 'You have selected to pay using PayPal as your payment method.'}}
			</p>
			<p>
				{{translate 'To review your order, click the "Continue" button below.'}}
			</p>
		{{else}}
			<p>
				{{translate 'Please select the "Continue To PayPal" button below to sign in into your PayPal account.'}}
			</p>
			<p>
				{{translate 'You will be redirected to PayPal, but will have an opportunity to review your order back on our site before purchasing.'}}
			</p>
		{{/if}}
	</div>
</div>

 <div class="order-wizard-paymentmethod-invoice-module-row">
		<label for="purchase-order-number" class="order-wizard-paymentmethod-invoice-module-purchase-order-label">
			{{translate 'Purchase Order Number'}}
		 <span class="po-edit-fields-group-label-required">(Optional)</span>
		</label>
		<input
			type="text"
			name="purchase-order-number"
			id="purchase-order-number"
			class="order-wizard-paymentmethod-invoice-module-purchase-order-value"
			value="{{purchaseNumber}}"
		>
	</div>
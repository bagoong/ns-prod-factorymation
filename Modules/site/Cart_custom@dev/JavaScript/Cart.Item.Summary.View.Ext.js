/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module Cart.Item.Summary.View.Ext
define('Cart.Item.Summary.View.Ext'
, [
    'Cart.Item.Summary.View'
  ,	'cart_item_summary_ext.tpl'
  , 'underscore'
  , 'jQuery'
  , 'Utils'
]
, function (
		View
  ,	cart_item_summary_ext_tpl
	, _
	, jQuery
	, Utils
  )
{
    'use strict';

    _.extend(View.prototype, {

      // @property {Function} Extended template added
      template: cart_item_summary_ext_tpl

			// @property {Function} add backorder attributes to getContext
	  , getContext: _.wrap(View.prototype.getContext, function (fn) {
        var ret = fn.apply(this, _.toArray(arguments).slice(1));
        var item = ret.line.get('item');
        ret.outofstockmessage = item.outofstockmessage;
        return ret;
      })
    });

})

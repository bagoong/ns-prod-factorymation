define(
    'CartGetContextExtension.View'
    , [
        'Cart.Summary.View'
        , 'underscore'
        , 'jQuery'
        , 'Utils'
    ]
    , function (CartSummaryView
        , _, jQuery, Utils) {
        'use strict';
        _.extend(CartSummaryView.prototype, {
            getContext: _.wrap(CartSummaryView.prototype.getContext, function (fn) {

                var ret = fn.apply(this, _.toArray(arguments).slice(1));
                var summary = ret.model.get('summary') || null;

                var shippingHandling = "$0.00";
                if(!!summary){
                    shippingHandling = Utils.formatCurrency(summary.shippingcost + summary.handlingcost);
                } else {
                    shippingHandling = Utils.formatCurrency(0);
                }
                ret.shippingHandlingCost_formatted = shippingHandling;

                return ret;

            })

        });
    });



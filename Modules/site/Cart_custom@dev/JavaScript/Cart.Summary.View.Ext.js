/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Cart.Summary.View.Ext
define(
	'Cart.Summary.View.Ext'
,	[
		'Cart.Summary.View'
	, 'cart_summary.tpl'
	, 'GlobalViews.Message.View'
	,	'underscore'
	,	'Backbone'
	]
,	function (
	 	CartSummaryView
	, cart_summary_tpl
	, GlobalViewsMessageView
	,	_
	,	Backbone
	)
{
	'use strict';

	// @class Cart.Summary.View @extends Backbone.View
	_.extend(CartSummaryView.prototype, {

			events: {
			'click [data-action="proceed-to-checkout"]': 'proceedToCheckout'
			}

		,	proceedToCheckout: function (e) {
			this.hideError();
				if (jQuery("div").find(".item-views-cell-actionable-stock-notice").length > 0) { // There is a stock error, unable proceed.
					this.showError(e);
					e.stopPropagation();
					e.preventDefault();
				}
			}

			// @method hideError
		,	hideError: function (selector)
			{
				var el = (selector)? selector.find('[data-type="alert-placeholder-stock"]') : this.$('[data-type="alert-placeholder-stock"]');
				el.empty();
			}

			// @method showError
		,	showError: function (e)
			{
				var placeholder;

				this.hideError();

				placeholder = this.$('[data-type="alert-placeholder-stock"]');

				var global_view_message = new GlobalViewsMessageView({
						message: 'Unavailable quantities. Please review items.'
					,	type: 'error'
					,	closable: true
				});

				// Renders the error message and into the placeholder
				placeholder.append(global_view_message.render().$el.html());
			}

	});
});

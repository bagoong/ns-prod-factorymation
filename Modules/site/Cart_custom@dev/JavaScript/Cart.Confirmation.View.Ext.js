define('Cart.Confirmation.View.Ext'
, [
    'Cart.Confirmation.View'
  , 'Backbone.CompositeView'
  ,	'cart_confirmation_modal_ext.tpl'
  , 'underscore'
  , 'jQuery'
  , 'Utils'
]
, function (
		View
  , BackboneCompositeView
  ,	cart_confirmation_modal_ext_tpl
	, _
	, jQuery
	, Utils
  )
{
    'use strict';

    _.extend(View.prototype, {

      // @property {Function} Extended template added
      template: cart_confirmation_modal_ext_tpl

    , modalClass: 'global-views-modal-small'
    ,	initialize: function (options)
  		{
  			this.model = options.model;
  			this.line = this.model.getLatestAddition();

  			var self = this
  			,	optimistic = this.model.optimistic;

  			if (optimistic && optimistic.promise && optimistic.promise.state() === 'pending')
  			{
  				this.line = options.model.optimisticLine;
  				delete this.model.optimisticLine;

  				optimistic.promise.done(function ()
  				{
  					self.line = options.model.getLatestAddition();
  					self.render();
  				});
  			}

  			BackboneCompositeView.add(this);
        this.cartImgSource = _.getAbsoluteUrl('img/added-to-cart_animation.gif')+ '?src=' + Math.random();


  		}

			// @property {Function} add backorder attributes to getContext
	  , getContext: _.wrap(View.prototype.getContext, function (fn) {
        var ret = fn.apply(this, _.toArray(arguments).slice(1));
        ret.addToCartImgSource = this.cartImgSource;
        return ret;
      })
    });

});

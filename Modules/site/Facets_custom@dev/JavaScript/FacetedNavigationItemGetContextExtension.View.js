define(
    'FacetedNavigationItemGetContextExtension.View'
    , [
        'Facets.FacetedNavigationItem.View'
        , 'underscore'
        , 'jQuery'
        , 'Utils'
    ]
    , function (FacetedNavigationItemView
        , _, jQuery, Utils) {
        'use strict';
        _.extend(FacetedNavigationItemView.prototype, {
            getContext: _.wrap(FacetedNavigationItemView.prototype.getContext, function (fn) {
                var ret = fn.apply(this, _.toArray(arguments).slice(1));
                var showExtras = false;
                _.each(ret.displayValues, function(value)
                {
                    if (value.url !== '') {
                        if(value.isActive){
                            ret.isCollapsed = false;
                        }
                    }
                });
                _.each(ret.extraValues, function(value)
                {
                    if (value.url !== '') {
                        if(value.isActive){
                            showExtras = true;
                            ret.isCollapsed = false;
                        }
                    }
                });
                ret.showExtras = showExtras;

                var facet_config = this.facet_config;
                var rangeTo;
                var rangeFrom;
                if(!_.isNumber(ret.rangeTo)){
                    rangeTo = (Number(ret.rangeTo)-1)+"";
                    rangeFrom = (Number(ret.rangeFrom)+1)+"";
                }else{
                    rangeTo = ret.rangeTo;
                    rangeFrom = ret.rangeFrom;
                }
                var extended_range_to_label = _.isFunction(facet_config.parser) ? facet_config.parser(rangeTo, false) : rangeTo;
                var extended_range_from_label = _.isFunction(facet_config.parser) ? facet_config.parser(rangeFrom, false) : rangeFrom;

                ret.showPriceSlider= ret.facetId == "pricelevel5";

                ret.extendedRangeFromLabel = extended_range_from_label!=0?extended_range_from_label.split(' ')[0]:0;
                ret.extendedRangeToLabel = extended_range_to_label!=0?extended_range_to_label.split(' ')[0]:0;



                return ret;

            })

        });
    });



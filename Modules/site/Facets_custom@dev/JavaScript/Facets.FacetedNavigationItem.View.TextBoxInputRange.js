define(
    'Facets.FacetedNavigationItem.View.TextBoxInputRange'
    , [
        'Facets.FacetedNavigationItem.View'
        , 'underscore'
        , 'jQuery'
        , 'Utils'
    ]
    , function (FacetedNavigationItemView
        , _, jQuery, Utils) {
        'use strict';
        _.extend(FacetedNavigationItemView.prototype, {
            initialize: function () {
                this.facetId = this.model.get('url') || this.model.get('id');
                this.facet_config = this.options.translator.getFacetConfig(this.facetId);

                //this values is configured in the Configuration File (SCA.Shopping.Configuration)
                if (this.facet_config.template) {
                    this.template = this.facet_config.template;
                }
                this.on('afterViewRender', this.renderFacets, this);
            },
            updateRange: function (e) {
                //if (e.key=="Enter") {
                    var minRangeInput = '#minRange-'+this.facetId;
                    var maxRangeInput = '#maxRange-'+this.facetId;
                    var minRange = $(minRangeInput).val()!=""?$(minRangeInput).val():$(minRangeInput)[0].placeholder.split(' ')[0];
                    var maxRange = $(maxRangeInput).val()!=""?$(maxRangeInput).val():$(maxRangeInput)[0].placeholder.split(' ')[0];
                    var translator = this.options.translator;
                    Backbone.history.navigate(translator.cloneForFacetId(this.facetId, {
                        from: (Number(minRange)-1),
                        to: (Number(maxRange)+1)
                    }).getUrl(), {trigger: true});

                //}

            },
            events: _.extend(FacetedNavigationItemView.prototype.events, {
                'change [name="minRange"]': 'updateRange',
                'change [name="maxRange"]': 'updateRange'
            }),


        });
    });



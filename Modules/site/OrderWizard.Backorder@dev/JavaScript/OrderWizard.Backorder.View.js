/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Cart
define('OrderWizard.Backorder.View'
,	[
		'ItemViews.Price.View'
	,	'Backbone.CompositeView'
	,	'ItemViews.SelectedOption.View'
	,	'Backbone.CollectionView'

	,	'orderwizard_backorder_modal.tpl'

	,	'jQuery'
	,	'Backbone'
	,	'underscore'
	,	'Utils'
	]
,	function (
		ItemViewsPriceView
	,	BackboneCompositeView
	,	ItemViewsSelectedOptionView
	,	BackboneCollectionView

	,	orderwizard_backorder_modal_tpl

	,	jQuery
	,	Backbone
	,	_
	)
{
	'use strict';

	// @class Cart.Confirmation.View Cart Confirmation view @extends Backbone.View
	return Backbone.View.extend({

		// @property {Function} template
		template: orderwizard_backorder_modal_tpl

		// @property {String} title
	,	title: _('Place Order').translate()

	,	modalClass: 'global-views-modal-large'

		// @property {String} page_header
	,	page_header: _('Place Order').translate()

		// @property {Object} attributes
	,	attributes: {
			'id': 'shopping-cart'
		,	'class': 'add-to-orderwizard-confirmation-modal shopping-orderwizard-modal'
		}

		// @property {Object} events
	,	events: {
			'click [data-trigger=go-to-cart]': 'dismisAndGoToCart'
		}

		// @method initialize
	,	initialize: function (options)
		{
			this.model = options.model;
			this.line = this.model.getLatestAddition();

			var self = this
			,	optimistic = this.model.optimistic;

			if (optimistic && optimistic.promise && optimistic.promise.state() === 'pending')
			{
				this.line = options.model.optimisticLine;
				delete this.model.optimisticLine;

				optimistic.promise.done(function ()
				{
					self.line = options.model.getLatestAddition();
					self.render();
				});
			}

			BackboneCompositeView.add(this);
		}

		// @method dismisAndGoToCart
		// Closes the modal and calls the goToCart
	,	dismisAndGoToCart: function (e)
		{
			e.preventDefault();
			this.$containerModal.modal('hide');
			this.options.layout.goToCart();
		}

		// @property {Object} childViews
	,	childViews: {
				'Item.Price': function ()
				{
				return new ItemViewsPriceView({
					model: this.line.get('item')
				,	origin: 'PDPCONFIRMATION'
				});
				}
			,	'Item.SelectedOptions': function ()
				{
					return new BackboneCollectionView({
						collection: new Backbone.Collection(this.line.get('item').getPosibleOptions())
					,	childView: ItemViewsSelectedOptionView
					,	viewsPerRow: 1
					,	childViewOptions: {
							cartLine: this.line
						}
					});
				}
		}

		// @method getContext
		// @return {Cart.Confirmation.View.Context}
	,	getContext: function()
		{
			var item = this.line.get('item');

			// @class Cart.Confirmation.View.Context
			return {
					// @property {OrderLine.Model} line
					line: this.line
					// @property {ItemDetails.Model} item
				,	item: item
					// @property {Boolean} showQuantity
				,	showQuantity: (item.get('_itemType') !== 'GiftCert') && (this.line.get('quantity') > 0)
					//@property {String} itemPropSku
				,	itemPropSku: (item.get('_sku'))
			};
		}
		// @class Cart.Confirmation.View
	});

});


/*define('LiveOrder.Backorders', ['Application'], function(Application) {
  //Application.on('before:LiveOrder.submit', function(Model, parameters) {
  checkBackorders : function() {
    // Error to be thrown if necessarry
    var orderPermission = {
      status: 400,
      code: 'ERR_LIVE_ORDER',
      message: ''
    };


    var lines = Model.get().lines;
    var errors = [];

    // Each Ordered item
    _.each(lines, function(line) {
      var vOutofstockbehavior = line.item.matrix_parent ?
      line.item.matrix_parent.outofstockbehavior : line.item.outofstockbehavior;

      nlapiLogExecution('DEBUG', 'item.quantity > item.quantityavailable:', line.quantity + '>' + line.item.quantityavailable);
      nlapiLogExecution('DEBUG', 'item', line.item.quantity + '>' + line.item.quantityavailable);

      if (line.quantity > line.item.quantityavailable) {
        nlapiLogExecution('DEBUG', 'Im in', 'not enough items');

        if (line.item.matrix_parent) {
          nlapiLogExecution('DEBUG', 'Im in matrix', 'mmmmmmmmmmmmm');

          errors.push({

            // quantityavailable is a field retrieved from the backend's ORDER
            // field set. It describes for each item the amount of stock
            // available for that particular item
            'backordered': line.quantity - line.item.quantityavailable,

            // This is the item name
            'item': line.item.matrix_parent.itemid || line.item.matrix_parent.pagetitle ||
             line.item.purchasedescription
          });
        } else {
          nlapiLogExecution('DEBUG', 'Im in item', 'aaaaaaaaaaaaaaaa');

          errors.push({

            'backordered': line.quantity - line.item.quantityavailable,
            // This is the item name
            'item': line.item.itemid || line.item.pagetitle || line.item.purchasedescription
          });
        }

      }
    });

    nlapiLogExecution('DEBUG', 'ERRORS.LENGTH', errors.length);

    if (errors.length > 0) {
      var errorMessage = "Some items have partial quantities available for immediate shipment.Remaining quantities will be backordered and ship once stock <br>becomes available for the following: ";
      _.each(errors, function(error, index) {
        errorMessage +=
          '<br><span style="color: #005daa;" ><b>' + error.item +
          ' - </b>' + error.backordered + ' items backordered.' +
          '</span>';
        if (index != errors.length - 1) {
          errorMessage += ' / ';
        }
      });
      errorMessage += '<br><br><div><a href="#" class="order-wizard-submitbutton-module-button" data-touchpoint="viewcart" data-hashtag="#cart" data-action="view-cart">Edit Order</a>';
      errorMessage += '<a href="#" class="order-wizard-submitbutton-module-button" data-action="submit-step">Confirm Order</a></div></div>';
      orderPermission.message = errorMessage;
      //throw orderPermission;
    }
  }
  //});
});*/

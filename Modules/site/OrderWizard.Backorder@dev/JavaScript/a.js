define('LiveOrder.Backorders', ['Application'], function(Application) {
  //Application.on('before:LiveOrder.submit', function(Model, parameters) {
  checkBackorders : function() {
    // Error to be thrown if necessarry
    var orderPermission = {
      status: 400,
      code: 'ERR_LIVE_ORDER',
      message: ''
    };


    var lines = Model.get().lines;
    var errors = [];

    // Each Ordered item
    _.each(lines, function(line) {
      var vOutofstockbehavior = line.item.matrix_parent ?
      line.item.matrix_parent.outofstockbehavior : line.item.outofstockbehavior;

      nlapiLogExecution('DEBUG', 'item.quantity > item.quantityavailable:', line.quantity + '>' + line.item.quantityavailable);
      nlapiLogExecution('DEBUG', 'item', line.item.quantity + '>' + line.item.quantityavailable);

      if (line.quantity > line.item.quantityavailable) {
        nlapiLogExecution('DEBUG', 'Im in', 'not enough items');

        if (line.item.matrix_parent) {
          nlapiLogExecution('DEBUG', 'Im in matrix', 'mmmmmmmmmmmmm');

          errors.push({

            // quantityavailable is a field retrieved from the backend's ORDER
            // field set. It describes for each item the amount of stock
            // available for that particular item
            'backordered': line.quantity - line.item.quantityavailable,

            // This is the item name
            'item': line.item.matrix_parent.itemid || line.item.matrix_parent.pagetitle ||
             line.item.purchasedescription
          });
        } else {
          nlapiLogExecution('DEBUG', 'Im in item', 'aaaaaaaaaaaaaaaa');

          errors.push({

            'backordered': line.quantity - line.item.quantityavailable,
            // This is the item name
            'item': line.item.itemid || line.item.pagetitle || line.item.purchasedescription
          });
        }

      }
    });

    nlapiLogExecution('DEBUG', 'ERRORS.LENGTH', errors.length);

    if (errors.length > 0) {
      var errorMessage = "Some items have partial quantities available for immediate shipment.Remaining quantities will be backordered and ship once stock <br>becomes available for the following: ";
      _.each(errors, function(error, index) {
        errorMessage +=
          '<br><span style="color: #005daa;" ><b>' + error.item +
          ' - </b>' + error.backordered + ' items backordered.' +
          '</span>';
        if (index != errors.length - 1) {
          errorMessage += ' / ';
        }
      });
      errorMessage += '<br><br><div><a href="#" class="order-wizard-submitbutton-module-button" data-touchpoint="viewcart" data-hashtag="#cart" data-action="view-cart">Edit Order</a>';
      errorMessage += '<a href="#" class="order-wizard-submitbutton-module-button" data-action="submit-step">Confirm Order</a></div></div>';
      orderPermission.message = errorMessage;
      //throw orderPermission;
    }
  }
  //});
});

/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module KnowledgeBase
define(
	'KnowledgeBase.Router'
,	[
		'KnowledgeBase.View'
	,	'KnowledgeBase.Topic.View'
	,	'KnowledgeBase.Solution.View'
	,	'KnowledgeBase.Collection'
	,	'KnowledgeBase.Model'
	,	'Newsroom.Solution.List.View'
	,	'Newsroom.Solution.Details.View'
	,	'AjaxRequestsKiller'

	,	'Backbone'
	]
,	function (
		KnowledgeBaseView
	,	KnowledgeBaseTopicView
	,	KnowledgeBaseSolutionView
	,	Collection
	,	Model
	,	NewsroomSolutionListView
	,	NewsroomSolutionDetailsView
	,	AjaxRequestsKiller

	,	Backbone
	)
{
	'use strict';

	// @lass Home.Router @extends Backbone.Router
	return Backbone.Router.extend({

		routes: {
			'knowledge-base': 'getTopicList',
			'knowledge-base/:id': 'getTopicList',
			'topic/:id' : 'getSolutionList',
			'solution/:id' : 'getSolutionDetails',
			'news-room' : 'getNewsroomSolutionList',
			'news-room/:id' : 'getNewsroomSolutionDetails'
		}

	,	initialize: function (Application)
		{
			this.application = Application;
		}

		// @method homePage dispatch the 'go to home page' route
	,	getTopicList: function (internalid)
		{
			this.requestPage('knowledgebase', KnowledgeBaseView, internalid);
		}

	,	getSolutionList: function(internalid)
		{
			this.requestPage('topic', KnowledgeBaseTopicView, internalid);
		}

	,	getSolutionDetails: function(internalid)
		{
			this.requestPage('solution', KnowledgeBaseSolutionView, internalid);
		}

	,	getNewsroomSolutionList: function()
		{
			this.requestPage('newsroom', NewsroomSolutionListView)
		}

	,	getNewsroomSolutionDetails: function(internalid)
		{
			this.requestPage('newsroomdetails', NewsroomSolutionDetailsView, internalid)
		}

	,	requestPage: function(pagetype, pageview, internalid)
		{
			var self = this
			,	collection = new Collection()
			,	model = new Model()
			,	view = new pageview({
					application: this.application
				,	collection: collection
				,	model: model
				});

			collection
				.on('reset', view.showContent, view)
				.fetch({
					data: {
						page: pagetype,
						id: internalid
					}
					,	killerId: AjaxRequestsKiller.getKillerId()
				}).done(function(data) {
					if (data.list && data.list.issubtopic) {
						view = new KnowledgeBaseView({
							application: self.application
							,	collection: collection
							,	model: model
						});
					}

					view.showContent();

				}).error(function() {
					view.showContent();
				});
		}
	});
});
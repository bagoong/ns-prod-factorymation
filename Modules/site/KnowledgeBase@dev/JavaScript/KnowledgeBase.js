/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Home
define(
	'KnowledgeBase'
,	[
		'KnowledgeBase.Router'
	]
,	function (
		KnowledgeBaseRouter
	)
{
	'use strict';

	//@class KnowledgeBase @extends ApplicationModule
	return {
		mountToApp: function (application)
		{
			return new KnowledgeBaseRouter(application);
		}
	};
	
});

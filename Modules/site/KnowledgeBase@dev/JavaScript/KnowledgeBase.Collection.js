/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module KnowledgeBase
define('KnowledgeBase.Collection'
,	[	'KnowledgeBase.Model'
	,	'underscore'
	,	'Backbone'
	,	'Utils'
	]
,	function (
		Model
	,	_
	,	Backbone
	)
{
	'use strict';
	//@class KnowledgeBase.Collection @extends Backbone.Collection
	return Backbone.Collection.extend({
		//@property {KnowledgeBase.Model} model
		model: Model
		//@property {String} url
	,	url: _.getAbsoluteUrl('services/KnowledgeBase.Service.ss')

	});
});

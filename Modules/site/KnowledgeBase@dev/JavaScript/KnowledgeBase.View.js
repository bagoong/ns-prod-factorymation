/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module KnowledgeBase
define(
	'KnowledgeBase.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'knowledge_base.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	knowledge_base_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module KnowledgeBase.View @extends Backbone.View
	return Backbone.View.extend({

		template: knowledge_base_tpl

	,	title: _('Knowledge Base').translate()

	,	attributes: {
			'id': 'knowledge-base-page'
		,	'class': 'knowledge-base-page'
		}

	,	events:
		{
			'': ''
		}

	,	initialize: function ()
		{

		}

    ,   childViews: {}

	,	destroy: function()
		{

		}

		//@method getBreadcrumbPages
	,	getBreadcrumbPages: function ()
		{
			var collection = this.collection.models
			,	breadcrumbs = [];

			breadcrumbs.push({
					text: _('Knowledge Base').translate()
				,	href: '#knowledge-base'
			});

			jQuery.each(collection, function(index, topic) {
				if (topic.get('list')) {
					if (topic.get('list').issubtopic) {
						breadcrumbs.push({
							text: _(topic.get('list').title).translate()
							,	href: ''
						});
					}
				}

				return false;
			});

			return breadcrumbs;
		}

		// @method getContext @return Home.View.Context
	,	getContext: function()
		{
			var collection = this.collection.models
			,	topics = [], categories = [];

			jQuery.each(collection, function(index, topic) {
				if (topic.get('list')) {
					categories = topic.get('categories');

					jQuery.each(topic.get('list').subtopics, function(idx, subtopic){
						topics.push({
							id: subtopic.id,
							name: subtopic.name,
							description: subtopic.description
						});
					});
				} else {
					topics.push({
						id: topic.get('id'),
						name: topic.get('name'),
						description: topic.get('description')
					});

					categories = topics;
				}

			});

			return {
				topics: topics,
				categories: categories
			};
		}

	});



});

/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module KnowledgeBase
define(
	'Newsroom.Solution.Details.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'newsroom_solution_details.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	newsroom_solution_details

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module KnowledgeBase.View @extends Backbone.View
	return Backbone.View.extend({

		template: newsroom_solution_details

	,	title: _('Solution').translate()

	,	attributes: {
			'id': 'newsroom-details-page'
		,	'class': 'newsroom-details-page'
		}

	,	initialize: function ()
		{

		}

		//@method getBreadcrumbPages
	,	getBreadcrumbPages: function ()
		{
			var self = this
			,	collection = this.collection.models
			,	breadcrumbs = [];

			jQuery.each(collection, function(index, solution) {
				// get topic if any
				if (solution.get('topic').id && solution.get('topic').name) {
					breadcrumbs.push({
							text: _(solution.get('topic').name).translate()
						,	href: '/news-room'
					});
				}

				// get solution
				jQuery.each(solution.get('solution'), function(idx, sol){
					breadcrumbs.push({
						text: _(sol.code).translate()
						,	href: '/news-room/' + sol.id
					});

					// update title
					self.title = sol.title
				});


				return false;
			});

			return breadcrumbs;
		}

		// @method getContext @return Home.View.Context
	,	getContext: function()
		{
			var collection = this.collection.models
			,	solutions = [], lists = [], categories = [], topic = {};

			jQuery.each(collection, function(index, sol) {
				lists = sol.get('solution');
				categories = sol.get('categories');
				topic = sol.get('topic');

				jQuery.each(lists, function(index, solution) {
					solutions.push({
						id: solution.id,
						code: solution.code,
						title: solution.title,
						message: solution.message,
						links: [
							{
								linkenable: solution.links[0].linkenable,
								linkimage: solution.links[0].linkimage,
								link: solution.links[0].link
							}
							,	{
								linkenable: solution.links[1].linkenable,
								linkimage: solution.links[1].linkimage,
								link: solution.links[1].link
							}
							,	{
								linkenable: solution.links[2].linkenable,
								linkimage: solution.links[2].linkimage,
								link: solution.links[2].link
							}
						],
						description: solution.description
					})
				});
			});

			//console.log('topics', topics);

			return {
				solution: solutions,
				categories: categories,
				topic: topic
			};
		}

	});

});

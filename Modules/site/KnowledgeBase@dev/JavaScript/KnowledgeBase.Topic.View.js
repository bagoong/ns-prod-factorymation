/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module KnowledgeBase
define(
	'KnowledgeBase.Topic.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'knowledge_base_topic.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	knowledge_base_topic_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module KnowledgeBase.View @extends Backbone.View
	return Backbone.View.extend({

		template: knowledge_base_topic_tpl

	,	title: _('Topic').translate()

	,	attributes: {
			'id': 'topic-page'
		,	'class': 'topic-page'
		}

	,	events:
		{
			'': ''
		}

	,	initialize: function ()
		{

		}

		//@method getBreadcrumbPages
	,	getBreadcrumbPages: function ()
		{
			var collection = this.collection.models
			,	breadcrumbs = [], topic = [];

			breadcrumbs.push({
					text: _('Knowledge Base').translate()
				,	href: '#knowledge-base'
			});

			jQuery.each(collection, function(index, col) {
				topic = col.get('topic');

				if (topic.parentname && topic.parentid) {
					breadcrumbs.push({
						text: _(topic.parentname).translate()
						,	href: '/topic/' + topic.parentid
					});
				}

				breadcrumbs.push({
					text: _(topic.name).translate()
					,	href: '/topic/' + topic.id
				});


					return false;
				});

			return breadcrumbs;
		}

		// @method getContext @return Home.View.Context
	,	getContext: function()
		{
			var collection = this.collection.models
			,	topics = [], lists = [], categories = [];

			jQuery.each(collection, function(index, col) {
				lists = col.get('list');
				categories = col.get('categories');

				if (lists.length) {
					jQuery.each(lists, function (index, topic) {
						topics.push({
							id: topic.id,
							code: topic.code,
							title: topic.title,
							message: topic.message,
							links: [
								{
									linkenable: topic.links[0].linkenable,
									linkimage: topic.links[0].linkimage,
									link: topic.links[0].link
								}
								, {
									linkenable: topic.links[1].linkenable,
									linkimage: topic.links[1].linkimage,
									link: topic.links[1].link
								}
								, {
									linkenable: topic.links[2].linkenable,
									linkimage: topic.links[2].linkimage,
									link: topic.links[2].link
								}
							]
						});
					});
				}
			});

			return {
				topics: topics,
				categories: categories
			};
		}

	});



});

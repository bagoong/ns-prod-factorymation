/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module KnowledgeBase
define(
	'Newsroom.Solution.List.View'
,	[
		'SC.Configuration'
	,	'Utilities.ResizeImage'

	,	'newsroom_solution_list.tpl'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	,	'Utils'
	]
,	function (
		Configuration

	,	resizeImage
	,	newsroom_solution_list_tpl

	,	Backbone
	,	jQuery
	,	_
	,	Utils
	)
{
	'use strict';

	//@module KnowledgeBase.View @extends Backbone.View
	return Backbone.View.extend({

		template: newsroom_solution_list_tpl

	,	title: _('Newsroom').translate()

	,	attributes: {
			'id': 'newsroom-page'
		,	'class': 'newsroom-page'
		}

	,	initialize: function ()
		{

		}

		//@method getBreadcrumbPages
	,	getBreadcrumbPages: function ()
		{
			var collection = this.collection.models
			,	breadcrumbs = [], topic = [];

			jQuery.each(collection, function(index, col) {
				topic = col.get('topic');
				breadcrumbs.push({
					text: _(topic.name).translate()
				});

				return false;
			});

			return breadcrumbs;
		}

		// @method getContext @return Home.View.Context
	,	getContext: function()
		{
			var collection = this.collection.models
			,	solutions = [], lists = [], categories = [], topic = {};

			jQuery.each(collection, function(index, col) {
				lists = col.get('list');
				categories = col.get('categories');
				topic = col.get('topic');

				if (lists.length) {
					jQuery.each(lists, function (index, solution) {
						solutions.push({
							id: solution.id,
							code: solution.code,
							title: solution.title,
							message: solution.message,
							links: [
								{
									linkenable: solution.links[0].linkenable == 'T' ? true : false,
									linkimage: solution.links[0].linkimage,
									link: solution.links[0].link
								}
								, {
									linkenable: solution.links[1].linkenable == 'T' ? true : false,
									linkimage: solution.links[1].linkimage,
									link: solution.links[1].link
								}
								, {
									linkenable: solution.links[2].linkenable == 'T' ? true : false,
									linkimage: solution.links[2].linkimage,
									link: solution.links[2].link
								}
							]
						});
					});
				}
			});

			return {
				solutions: solutions,
				categories: categories,
				topic: topic

			};
		}

	});



});

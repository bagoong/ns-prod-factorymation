{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="knowledge-base-container">

	<div class="col-md-3">
        <br>
	    <div class="side-menu">
            <ul class="header-menu-level1 nav navbar-nav side-bar-landings">
               <li class="dropdown">
                   <span class="header-menu-shop-anchor" data-toggle="dropdown" aria-expanded="false">
                        {{translate 'Categories'}}
                        <i class="icon-arrow-dropdown"></i>
                   </span>
                   <ul class="header-menu-level2 dropdown-menu">
                        {{#each categories}}
                            <li class="dropdown dropdown-submenu">
                               <a href="/topic/{{id}}">{{name}}</a>
                               {{#each subtopics}}
                                  <li class="dropdown dropdown-submenu sub-categ">
                                      <a href="/topic/{{id}}">{{name}}</a>
                                  </li>
                              {{/each}}
                            </li>
                       {{/each}}
                   </ul>
               </li>
            </ul>

        </div>
	</div>

	<div class="col-md-9"> <!-- class: col-md-9 -->
	    <img src="/../app/img/knowledge_base_header.jpg" />
	    <div class="header-divider"></div>
	    <div class="col-md-9">
            {{#each topics}}
               <div class="topic-row">
                    <div class="topic-list-title">
                        <a href="topic/{{id}}"><b>{{name}}</b></a>
                    </div>
                    {{{description}}}
               </div>
            {{/each}}
	    </div>
	</div>

</div>
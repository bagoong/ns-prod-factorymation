{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="newsroom-container">

	<div class="col-md-3">
	    <div class="side-menu">
            <ul class="header-menu-level1 nav navbar-nav side-bar-landings">
              <li class="dropdown">
                  <span class="header-menu-shop-anchor" data-toggle="dropdown" aria-expanded="false">
                       {{translate 'Categories'}}
                       <i class="icon-arrow-dropdown"></i>
                  </span>
                  <ul class="header-menu-level2 dropdown-menu">
                       <li class="dropdown dropdown-submenu">
                               <a href="/news-room">{{topic.name}}</a>
                           </li>

                          {{#each categories}}
                            <li class="dropdown dropdown-submenu sub-categ">
                                <a href="/news-room/{{id}}">{{code}}</a>
                            </li>
                          {{/each}}
                  </ul>
              </li>
           </ul>

        </div>
	</div>

	<div class="col-md-9"> <!-- class: col-md-9 -->
	    <img src="/../app/img/newsroom_header.gif" class="newsroom-header-img" />
        <div class="header-divider"></div>

        {{#each solution}}
            <div class="col-md-12">
                <div class="details-row">
                    <span class="label">{{translate 'Date:'}} </span> {{code}}
                </div>
                <div class="details-row">
                    <span class="label">{{translate 'Title:'}} </span> {{title}}
                </div>
                <div class="details-row">
                    <span class="label">{{translate 'Summary:'}} </span> {{{message}}}
                </div>
                <div class="details-row">
                    <span class="label">{{translate 'Detailed Description:'}} </span> {{{description}}}
                </div>
            </div>

        {{/each}}
	</div>

</div>
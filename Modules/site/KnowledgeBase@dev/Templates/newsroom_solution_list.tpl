{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="newsroom-container">

	<div class="col-md-3">
	    <div class="side-menu">
           <ul class="header-menu-level1 nav navbar-nav side-bar-landings">
              <li class="dropdown">
                  <span class="header-menu-shop-anchor" data-toggle="dropdown" aria-expanded="false">
                       {{translate 'Categories'}}
                       <i class="icon-arrow-dropdown"></i>
                  </span>
                  <ul class="header-menu-level2 dropdown-menu">
                        <li class="dropdown dropdown-submenu">
                            <a href="/news-room">{{topic.name}}</a>
                        </li>

                       {{#each categories}}
                         <li class="dropdown dropdown-submenu sub-categ">
                             <a href="/news-room/{{id}}">{{code}}</a>
                         </li>
                       {{/each}}
                  </ul>
              </li>
           </ul>

        </div>
	</div>

	<div class="col-md-9 newsroom-list-container"> <!-- class: col-md-9 -->
	    <img src="/../app/img/newsroom_header.gif" class="newsroom-header-img" />

        {{#each solutions}}
            <div class="row list-row">
                <div class="col-sm-2">
                    {{#each links}}
                        <div class="link-icon">
                           {{#if linkenable}}
                                {{#if link}}
                                    <a href="{{link}}" target="_blank">
                                {{else}}
                                    <a href="/news-room/{{../../../id}}" >
                                {{/if}}
                                        <img src="{{{linkimage}}}" />
                                    </a>
                           {{/if}}
                        </div>
                    {{/each}}
                </div>

                <div class="col-sm-10">
                    <div class="title"><a href="/news-room/{{id}}">{{title}}</a></div>
                    <div class="brief-description">{{{message}}} <a href="/news-room/{{id}}" class="more">{{translate 'more'}}</a></div>
                </div>
            </div>
            <hr />
        {{/each}}

	</div>

</div>
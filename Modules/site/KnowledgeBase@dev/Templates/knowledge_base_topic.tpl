{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="knowledge-base-container">

	<div class="col-md-3">
	    <div class="side-menu">
           <ul class="header-menu-level1 nav navbar-nav side-bar-landings">
              <li class="dropdown">
                  <span class="header-menu-shop-anchor" data-toggle="dropdown" aria-expanded="false">
                       {{translate 'Categories'}}
                       <i class="icon-arrow-dropdown"></i>
                  </span>
                  <ul class="header-menu-level2 dropdown-menu">
                       {{#each categories}}
                           <li class="dropdown dropdown-submenu">
                              <a href="/topic/{{id}}">{{name}}</a>
                              {{#each subtopics}}
                                 <li class="dropdown dropdown-submenu sub-categ">
                                     <a href="/topic/{{id}}">{{name}}</a>
                                 </li>
                             {{/each}}
                           </li>
                      {{/each}}
                  </ul>
              </li>
           </ul>

        </div>
	</div>

	<div class="col-md-9"> <!-- class: col-md-9 -->
	    <img src="/../app/img/knowledge_base_header.jpg" />

        <table class="table solution-list-table">
            <thead>
                <th class="col-md-2 table-header">{{translate 'Item Code'}}</th>
                <th class="col-md-3 table-header">{{translate 'Title'}}</th>
                <th class="col-md-7 table-header">{{translate 'Summary'}}</th>
            </thead>
            <tbody>
                {{#each topics}}
                    <tr>
                        <td><a href="/solution/{{id}}">{{code}}</a></td>
                        <td><a href="/solution/{{id}}">{{title}}</a></td>
                        <td>
                            <div class="col-md-9">
                                {{{message}}}
                            </div>
                            {{#each links}}
                                <div class="col-md-1 link-icon">
                                   {{#if link}}
                                        <a href="{{link}}" target="_blank"><img src="{{{linkimage}}} /"></a>
                                   {{/if}}
                                </div>
                            {{/each}}
                        </td>
                    </tr>
                {{/each}}
            </tbody>
        </table>

	</div>

</div>
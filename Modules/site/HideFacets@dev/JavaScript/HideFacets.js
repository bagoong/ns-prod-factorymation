define('HideFacets', [
    'Facets.FacetedNavigationItem.View',
    'underscore'

], function HideFacets(
    FacetsFacetedNavigationItemView,
    _
) {
    'use strict';
    _.extend(FacetsFacetedNavigationItemView.prototype, {
        getContext: _.wrap(FacetsFacetedNavigationItemView.prototype.getContext, function (fn) {
            var ret = fn.apply(this, _.toArray(arguments).slice(1));
            ret.showFacet = ret.showFacet && !this.facet_config.hide;
            return ret;
        })
    });
});
define('LiveOrder.StockError', ['Application'], function(Application) {
    Application.on('before:LiveOrder.submit', function(Model, parameters) {
        var orderPermission = {
            status: 400,
            code: 'ERR_LIVE_ORDER',
            message: ''
        };
        var lines = Model.get().lines;
        var errors = [];
        _.each(lines, function(line) {
            var vOutofstockbehavior = line.item.outofstockbehavior;
            if (line.item.matrix_parent) {
                vOutofstockbehavior = line.item.matrix_parent.outofstockbehavior;
            }
            if (line.quantity > line.item.quantityonhand) {
                if (line.item.matrix_parent) {
                    errors.push({
                        // quantityonhand is a field retrieved from the backend's ORDER field set. It describes for each item the amount of stock available for that particular item
                        'available': line.item.quantityonhand,
                        // This is the item name, particularly Mountz does not have a clear name field, but many fields that repeat the name of the item
                        'item': line.item.matrix_parent.salesdescription || line.item.matrix_parent.purchasedescription || line.item.matrix_parent.description || line.item.matrix_parent.displayname
                    });
                } else {
                    errors.push({
                        'available': line.item.quantityonhand,
                        // This is the item name, particularly Mountz does not have a clear name field, but many fields that repeat the name of the item
                        'item': line.item.salesdescription || line.item.purchasedescription || line.item.description || line.item.displayname
                    });
                }

            }
        });

        if (errors.length > 0) {
            var errorMessage = "We don’t have inventory available for immediate shipping. Please reduce your ordering quantity based on the current available inventory: ";
            _.each(errors, function(error, index) {
                errorMessage += '<br><span style="color: #005daa;" ><b>' + error.item + ' - </b>' + error.available + ' pieces available' + '</span>';
                if (index != errors.length - 1) {
                    errorMessage += ' / ';
                }
            });
            errorMessage += '<br><a href="#" class="btn btn-primary btn-large add-to-cart-btn" data-touchpoint="viewcart">Edit Order</a>';
            orderPermission.message = errorMessage;
            throw orderPermission;
        }
    });
});

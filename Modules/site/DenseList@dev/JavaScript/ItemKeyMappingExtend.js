define('ItemKeyMappingExtend', [
    'ItemsKeyMapping',
    'underscore',
    'SC.Configuration'
], function ItemImages(ItemsKeyMapping,
                       _,
                       Configuration) {
    'use strict';

    function extendKeyMapping() {
        Configuration.itemKeyMapping = Configuration.itemKeyMapping || {};

        _.extend(Configuration.itemKeyMapping, {
            _mustLTL: 'custitem45',
            _LTLmessage: 'custitem55'
        });
    }

    return {
        mountToApp: function mountToApp(application) {
            extendKeyMapping();
        }
    };
});
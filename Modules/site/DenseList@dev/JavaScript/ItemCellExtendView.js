define('ItemCellExtendView', [
    'Facets.ItemCell.View',
    'ItemViews.RelatedItem.View',
    'ItemDetails.View',
    'SC.Configuration',
    'LiveOrder.Model',

    'facets_item_cell_list.tpl',
    'Backbone.CollectionView',
    'Backbone',
    'ItemViews.Option.View',
    'ItemViews.Price.View',
    'underscore'
], function ItemCellExtend(FacetsItemCellView, ItemViewsRelatedItemView, ItemDetailsView, Configuration, LiveOrderModel, facets_item_cell_list_tpl,
                           BackboneCollectionView, Backbone, ItemViewsOptionView, ItemViewsPriceView,
                           _) {
    'use strict';

    delete ItemDetailsView.prototype.events['change [name="quantity"]'];
    delete ItemDetailsView.prototype.events['keypress [name="quantity"]'];
    _.extend(ItemDetailsView.prototype.events, {
        'change .item-details-add-to-cart-form input[name="quantity"]': 'updateQuantity',
        'keypress .item-details-add-to-cart-form input[name="quantity"]':'submitOnEnter'
    });

    _.extend(FacetsItemCellView.prototype, {
        getContext: _.wrap(FacetsItemCellView.prototype.getContext, extendItemDetails)
    });

    addMatrixItem(FacetsItemCellView);


    _.extend(ItemViewsRelatedItemView.prototype, {
        template: facets_item_cell_list_tpl,
        events: {
            'submit [data-toggle="add-to-cart"]': 'addToCartFromList'
        },
        getContext: _.wrap(ItemViewsRelatedItemView.prototype.getContext, extendItemDetails),
        childViews: _.extend(ItemViewsRelatedItemView.prototype.childViews || {}, {
            'ItemViews.Price': function ()
                {
                    return new ItemViewsPriceView({
                        model: this.model
                        ,	origin: 'RELATEDITEM'
                    });
                }
        }),
        addToCartFromList: function (e)
        {
            e.preventDefault();

            var options = jQuery(e.target).serializeObject()
                ,	model = this.model;

            // Updates the quantity of the model
            model.setOption('quantity', options.quantity);

            if (model.isReadyForCart())
            {
                var self = this
                    ,	cart = LiveOrderModel.getInstance()
                    ,	layout = this.options.application.getLayout()
                    ,	cart_promise = jQuery.Deferred()
                    ,	error_message = _('Sorry, there is a problem with this Item and can not be purchased at this time. Please check back later.').translate();



                if (model.cartItemId)
                {
                    cart_promise = cart.updateItem(model.cartItemId, model).done(function ()
                    {
                        if (cart.getLatestAddition())
                        {
                            if (self.$containerModal)
                            {
                                self.$containerModal.modal('hide');
                            }

                            if (layout.currentView instanceof require('Cart.Detailed.View'))
                            {
                                layout.currentView.showContent();
                            }
                        }
                        else
                        {
                            self.showError(error_message);
                        }
                    });
                }
                else
                {
                    cart_promise = cart.addItem(model).done(function ()
                    {
                        if (cart.getLatestAddition())
                        {
                            if (self.$containerModal)
                            {
                                self.$containerModal.modal('hide');
                            }

                            if (layout.currentView instanceof require('Cart.Detailed.View'))
                            {
                                layout.currentView.showContent();
                                layout.showCartConfirmation();
                            }else{
                                layout.showCartConfirmation();
                            }
                        }
                        else
                        {
                            self.showError(error_message);
                        }
                    });
                }

                // disalbes the btn while it's being saved then enables it back again
                if (e && e.currentTarget)
                {
                    jQuery('input[type="submit"]', e.currentTarget).attr('disabled', true);
                    cart_promise.always(function () {
                        jQuery('input[type="submit"]', e.currentTarget).attr('disabled', false);
                    });
                }
            }
        }
    });

    addMatrixItem(ItemViewsRelatedItemView);

    function addMatrixItem(view) {
        _.extend(view.prototype, {
            childViews: _.extend(view.prototype.childViews || {}, {
                'ItemDetails.Options': function()
                {
                    return new BackboneCollectionView({
                        collection: new Backbone.Collection(_.filter(this.model.getPosibleOptions(), function (opt) {
                            //{isMatrixDimension: true}
                            return true;
                        }))
                        ,	childView: ItemViewsOptionView
                        ,	viewsPerRow: 1
                        ,	childViewOptions: {
                            item: this.model,
                            tpl: 'dropdown'
                        }
                    });
                }
            })
            , setOptionExt: function asa(e) {
                var self = this
                    ,	$target = jQuery(e.currentTarget)
                    ,	value = $target.val() || $target.data('value') || null
                    ,	cart_option_id = $target.closest('[data-type="option"]').data('cart-option-id');

                // prevent from going away
                e.preventDefault();

                // if option is selected, remove the value
                if ($target.data('active'))
                {
                    value = null;
                }

                // it will fail if the option is invalid
                try
                {
                    this.model.setOption(cart_option_id, value);
                }
                catch (error)
                {
                    // Clears all matrix options
                    _.each(this.model.getPosibleOptions(), function (option)
                    {
                        option.isMatrixDimension && self.model.setOption(option.cartOptionId, null);
                    });

                    // Sets the value once again
                    this.model.setOption(cart_option_id, value);
                }

                this.render();


                // Need to trigger an afterAppendView event here because, like in the PDP, we are really appending a new view for the new selected matrix child
                if (this.$containerModal)
                {
                    this.application.getLayout().trigger('afterAppendView', this);
                }
            }
            , events: _.extend(view.prototype.events || {}, {
                'change [data-toggle="select-option"],[data-toggle="text-option"]': 'setOptionExt'
            })
        });
    }

    function extendItemDetails (fn) {
        var item = this.model;
        var details_object = item.get('_priceDetails') || {}
            , hasPriceSchedule = false
            , priceSchedule = [];

        if (details_object.priceschedule && details_object.priceschedule.length) {
            hasPriceSchedule = true;

            var price, min;

            for (var i = 0; i < details_object.priceschedule.length; i++) {
                price = details_object.priceschedule[i];
                min = parseInt(price.minimumquantity, 10);
                min = min == 0 ? 1 : min;
                priceSchedule.push({
                    price: price.price_formatted,
                    minimunQty: min
                });
            }
        }

        var result = _.extend(fn.apply(this, _.toArray(arguments).slice(1)), {
            //description: item.get('storedetaileddescription'),
            description: item.get('storedescription'),
            mustLTL: item.get('_mustLTL'),
            LTLimgSource: _.getAbsoluteUrl('img/LTL.PNG'),
            inStockImgSource: _.getAbsoluteUrl('img/in-stock.png'),
            inStock: item.get('isinstock'),
            outOfStockImgSource: _.getAbsoluteUrl('img/out-of-stock.png'),
            dropShipImgSource: _.getAbsoluteUrl('img/drop-ship-item.png'),
            dropShipItem: item.get('isdropshipitem'),
            isBuiltToOrderImgSource: _.getAbsoluteUrl('img/BuiltToOrder_icon.png'),
            isBuiltToOrder: item.get('custitem98'),
            specialOrderImgSource: _.getAbsoluteUrl('img/lead-time-item.png'),
            specialOrderItem: item.get('isspecialorderitem'),
            LTLMessage: item.get('_LTLmessage'),
            quickViewimgSource: _.getAbsoluteUrl('img/quick-view.png'),
            addToCart: _.getAbsoluteUrl('img/add-to-cart.png'),
            noCart: _.getAbsoluteUrl('img/no-cart.png'),
            hasPriceSchedule: hasPriceSchedule,
            stockMessage: item.get('outofstockmessage'),
            priceSchedule: priceSchedule,
            dontShowPrice: item.get('dontshowprice'),
            noPriceMessage: item.get('nopricemessage')
        });
        return _.extend({ // Defaulting for Related items
            name: this.model.get('_name'),
            url: this.model.get('_url'),
            thumbnail: this.model.get('_thumbnail'),
            rating: this.model.get('_rating'),
            minQuantity: parseInt(this.model.get('_minimumQuantity'), 10),
            stockInfo: this.model.getStockInfo(),
            canAddToCart: Configuration.addToCartFromFacetsView && this.model.isReadyForCart(),
            isEnvironmentBrowser: SC.ENVIRONMENT.jsEnvironment === 'browser' && !SC.ENVIRONMENT.isTouchEnabled,
            itemIsNavigable: !_.isUndefined(this.options.itemIsNavigable) ? !!this.options.itemIsNavigable : true,
            showRating: SC.ENVIRONMENT.REVIEWS_CONFIG && SC.ENVIRONMENT.REVIEWS_CONFIG.enabled
        }, result);
    };
});
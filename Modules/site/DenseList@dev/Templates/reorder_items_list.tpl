{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

{{#if showBackToAccount}}
	<a href="/" class="reorder-items-list-button-back">
		<i class="reorder-items-list-button-back-icon"></i>
		{{translate 'Back to Account'}}
	</a>
{{/if}}

<div class="reorder-items-list">
	<header class="reorder-items-list-hedaer">
		<h2>{{pageHeader}}</h2>
	</header>

	<div data-view="ListHeader"></div>
	<div class="facets-facet-browse-columns">
        <div class="facets-facet-browse-items-column">Item</div>
        <div class="facets-facet-browse-details-column">Item Details</div>
        <div class="facets-facet-browse-price-content-column">
            <div class="facets-facet-browse-price-column">Price</div>
            <div class="facets-facet-browse-qty-column">QTY</div>
            <div class="facets-facet-browse-stock-column">Stock Status</div>
            <div class="facets-facet-browse-addtocart-column">Add To Cart</div>
        </div>
    </div>
	{{#if showItems}}
		 <table class="reorder-items-list-reorder-items-table md2sm">
			<tbody data-view="Reorder.Items">
			</tbody>
		</table>
	{{/if}}
	{{#if itemsNotFound}}
		<div class="reorder-items-list-empty-section">
			<h5>{{translate 'You bought no items in this time period.'}}</h5>
			<p><a class="reorder-items-list-empty-button" href="#" data-touchpoint="home">{{translate 'Shop Now'}}</a></p>
		</div>
	{{/if}}

	{{#if isLoading}}
		<p class="reorder-items-list-empty">{{translate 'Loading...'}}</p>
	{{/if}}

	{{#if showPagination}}
		<div class="reorder-items-list-paginator">
			<div data-view="GlobalViews.Pagination"></div>
			{{#if showCurrentPage}}
				<div data-view="GlobalViews.ShowCurrentPage"></div>
			{{/if}}
		</div>
	{{/if}}
</div>

{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="facets-item-cell-list" itemprop="itemListElement" data-item-id="{{itemId}}" itemscope
     itemtype="http://schema.org/Product" data-track-productlist-list="{{track_productlist_list}}"
     data-track-productlist-category="{{track_productlist_category}}"
     data-track-productlist-position="{{track_productlist_position}}" data-sku="{{sku}}">
    <div class="facets-item-cell-list-item">
        {{#if itemIsNavigable}}
            <a class="facets-item-cell-list-anchor" href='{{url}}'>
                <img class="facets-item-cell-list-image" src="{{resizeImage thumbnail.url 'thumbnail'}}" alt="{{thumbnail.altimagetext}}" itemprop="image">
            </a>
        {{else}}
            <img class="facets-item-cell-list-image" src="{{resizeImage thumbnail.url 'thumbnail'}}" alt="{{thumbnail.altimagetext}}" itemprop="image">
        {{/if}}
        {{#if itemIsNavigable}}<a class="facets-item-cell-list-name" href='{{url}}' data-touchpoint="home" data-hashtag="#{{url}}">{{/if}}
        {{name}}
        {{#if itemIsNavigable}} </a> {{/if}}
    </div>
    <div class="facets-item-cell-list-details">
        <div class="facets-item-cell-list-description">
            {{{description}}}

            {{#if inStock}}
                {{#if dropShipItem}}
                    {{{stockMessage}}}
                {{/if}}
                {{#if specialOrderItem}}
                    {{{stockMessage}}}
                {{/if}}
                {{#if isBuiltToOrder}}
                    {{{stockMessage}}}
                {{/if}}
            {{else}}
                {{{stockMessage}}}
            {{/if}}

        </div>
        {{#if mustLTL}}
            <div class="facets-item-cell-list-LTL">
                <!--<img class="LTL-truck" src={{LTLimgSource}}>-->
                {{{LTLMessage}}}
            </div>
        {{/if}}

        {{#if isEnvironmentBrowser}}
            <div class="facets-item-cell-list-quick-view-wrapper">
                <a href="{{url}}" class="" data-toggle="show-in-modal">
                    <img class="quick-View" src={{quickViewimgSource}}>
                </a>
            </div>
        {{/if}}

    </div>
    <div class="facets-item-cell-list-price-container">
        <div class="facets-item-cell-list-price">
            {{#if hasPriceSchedule}}
                {{#each priceSchedule}}
                    <div class="facets-item-cell-list-min-qry">{{minimunQty}}+</div>
                    <div class="facets-item-cell-list-min-qry-price">{{price}}</div>
                {{/each}}
            {{else}}
                {{#if dontShowPrice}}
                    <div class="facets-item-cell-list-no-price-message">{{noPriceMessage}}</div>
                {{else}}
                    <div data-view="ItemViews.Price"></div>
                {{/if}}
            {{/if}}
        </div>
        <form class="facets-item-cell-list-add-to-cart" data-toggle="add-to-cart">
            <input class="facets-item-cell-list-add-to-cart-itemid" type="hidden" value="{{itemId}}" name="item_id">
            <div class="facets-item-cell-list-qty">
                <input class="facets-item-cell-list-add-to-cart-quantity" name="quantity" type="number" min="1"
                       value="{{minQuantity}}">
                <div data-view="ItemDetails.Options"></div>
            </div>
            <div class="facets-item-cell-list-stock">
                {{#if dropShipItem}}
                    <img class="facets-item-cell-stock-img" src="{{dropShipImgSource}}">
                {{else}}
                    {{#if specialOrderItem}}
                        <img class="facets-item-cell-stock-img" src="{{specialOrderImgSource}}">
                    {{else}}
                        {{#if isBuiltToOrder}}
                            <img class="facets-item-cell-stock-img" src="{{isBuiltToOrderImgSource}}">
                        {{else}}
                            {{#if inStock}}
                                <img class="facets-item-cell-stock-img" src="{{inStockImgSource}}">
                            {{else}}
                                <img class="facets-item-cell-stock-img" src="{{outOfStockImgSource}}">
                            {{/if}}
                        {{/if}}
                    {{/if}}
                {{/if}}
            </div>
        {{#if canAddToCart}}
            <div class="facets-item-cell-list-addtocart-button">
                {{#if dontShowPrice}}
                <!--<img class="header-mini-cart-menu-cart-icon icon-white" src="{{noCart}}">-->
                {{else}}
                <button class="add-to-cart-item-list" type="submit">
                    <img class="header-mini-cart-menu-cart-icon icon-white" src="{{addToCart}}">
                </button>
                {{/if}}
            </div>
        {{/if}}

        </form>

    </div>
</div>

<!--
<div class="facets-item-cell-list" itemprop="itemListElement"  data-item-id="{{itemId}}" itemscope itemtype="http://schema.org/Product" data-track-productlist-list="{{track_productlist_list}}" data-track-productlist-category="{{track_productlist_category}}" data-track-productlist-position="{{track_productlist_position}}" data-sku="{{sku}}">
	<div class="facets-item-cell-list-left">
		<div class="facets-item-cell-list-image-wrapper">
			{{#if itemIsNavigable}}
				<a class="facets-item-cell-list-anchor" href='{{url}}'>
					<img class="facets-item-cell-list-image" src="{{resizeImage thumbnail.url 'thumbnail'}}" alt="{{thumbnail.altimagetext}}" itemprop="image">
				</a>
			{{else}}
				<img class="facets-item-cell-list-image" src="{{resizeImage thumbnail.url 'thumbnail'}}" alt="{{thumbnail.altimagetext}}" itemprop="image">
			{{/if}}
    {{#if isEnvironmentBrowser}}
				<div class="facets-item-cell-list-quick-view-wrapper">
					<a href="{{url}}" class="facets-item-cell-list-quick-view-link" data-toggle="show-in-modal">
						<i class="facets-item-cell-list-quick-view-icon"></i>
						{{translate 'Quick View'}}
					</a>
				</div>
			{{/if}}
		</div>
	</div>
	<div class="facets-item-cell-list-right">
		<meta itemprop="url" content="{{url}}">
		<h2 class="facets-item-cell-list-title">
			{{#if itemIsNavigable}}
				<a class="facets-item-cell-list-name" href='{{url}}'>
					<span itemprop="name">
						{{name}}
					</span>
				</a>
			{{else}}
				<span itemprop="name">
					{{name}}
				</span>
			{{/if}}
		</h2>

		<div class="facets-item-cell-list-price">
			<div data-view="ItemViews.Price"></div>
		</div>

		{{#if showRating}}
		<div class="facets-item-cell-list-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"  data-view="GlobalViews.StarRating">
		</div>
		{{/if}}

		<div data-view="ItemDetails.Options"></div>

		{{#if canAddToCart}}
			<form class="facets-item-cell-list-add-to-cart" data-toggle="add-to-cart">
				<input class="facets-item-cell-list-add-to-cart-itemid" type="hidden" value="{{itemId}}" name="item_id">
				<input class="facets-item-cell-list-add-to-cart-quantity" name="quantity" type="number" min="1" value="{{minQuantity}}">
				<input class="facets-item-cell-list-add-to-cart-button" type="submit" value="{{translate 'Add to Cart'}}">
			</form>
		{{/if}}
		<div class="facets-item-cell-list-stock">
			<div data-view="ItemViews.Stock"></div>
		</div>
	</div>
</div>
-->

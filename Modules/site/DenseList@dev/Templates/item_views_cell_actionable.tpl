{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="facets-item-cell-list" itemprop="itemListElement" data-item-id="{{itemId}}" itemscope
     itemtype="http://schema.org/Product" data-sku="{{item._sku}}">
	<div class="facets-item-cell-list-item">
        {{#if isNavigable}}
            <a {{linkAttributes}}>
                <img src="{{resizeImage item._thumbnail.url 'thumbnail'}}" alt="{{item._thumbnail.altimagetext}}">
            </a>
        {{else}}
            <img src="{{resizeImage item._thumbnail.url 'thumbnail'}}" alt="{{item._thumbnail.altimagetext}}">
        {{/if}}
        {{#if isNavigable}}<a class="facets-item-cell-list-name" href='{{item._url}}' data-touchpoint="home" data-hashtag="#{{item._url}}">{{/if}}
        {{item._name}}

        {{#if isNavigable}} </a> {{/if}}
    </div>
    <div class="facets-item-cell-list-details">
        <div class="facets-item-cell-list-description">
            {{{item.storedescription2}}}



			{{#if item.custitem45}}
            <div class="facets-item-cell-list-LTL">
                {{{item.custitem55}}}
            </div>
            {{/if}}
            <div data-view="Item.SelectedOptions"></div>
            {{#if item.isinstock}}
                {{#if item.isdropshipitem}}
                    {{{item.outofstockmessage}}}
                {{/if}}
                {{#if item.isspecialorderitem}}
                    {{{item.outofstockmessage}}}
                {{/if}}
                {{#if item.custitem98}}
                    {{{item.outofstockmessage}}}
                {{/if}}
            {{else}}

                {{{item.outofstockmessage}}}

            {{/if}}
						{{#if obsolete}}
							<div class="item-views-cell-actionable-stock-notice" data-type="stock-placeholder">
								{{obsolete}}
							</div>
						{{/if}}

						{{#if showBackorderMsg}}
								<div class="item-views-cell-actionable-backorder-stock-notice" data-type="stock-placeholder">
									{{showBackorderMsg}}
								</div>
						{{/if}}
        </div>
    </div>



    <div class="facets-item-cell-list-price-container">
        <div class="facets-item-cell-list-price">
            {{#if hasPriceSchedule}}
                {{#each priceSchedule}}
                    <div class="facets-item-cell-list-min-qry">{{minimunQty}}+</div>
                    <div class="facets-item-cell-list-min-qry-price">{{price}}</div>
                {{/each}}
            {{else}}
                {{#if dontShowPrice}}
                    <div class="item-details-no-price-message">{{noPriceMessage}}</div>
                {{else}}
                    <div data-view="Item.Price"></div>
                {{/if}}
            {{/if}}
        </div>
        <form class="facets-item-cell-list-add-to-cart" data-toggle="add-to-cart">

            <div class="facets-item-cell-list-qty">
				{{#if showSummaryView}}
				<div class="item-views-cell-actionable-summary" data-view="Item.Summary.View"></div>
				{{/if}}
            </div>
        <div class="item-views-cell-actionable-options">
			<div data-view="Item.Actions.View"></div>
		</div>
        </form>

    </div>
{{#if showAlert}}
	<div class="item-views-cell-actionable-alert-placeholder" data-type="alert-placeholder"></div>
{{/if}}

{{#if showCustomAlert}}
	<div class="alert alert-{{customAlertType}}">
		{{item._cartCustomAlert}}
	</div>
{{/if}}
</div>

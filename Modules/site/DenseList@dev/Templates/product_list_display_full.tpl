{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="{{#if isChecked}}active{{/if}} facets-item-cell-list" data-id="{{itemId}}" data-item-id="{{itemDetailsId}}" data-action="product-list-item">
	{{#if showCheckbox}}
	<div class="product-list-display-full-select">
		<input type="checkbox" value="{{itemId}}" data-action="select" {{#if isChecked}}checked{{/if}}>
	</div>
	{{/if}}

	<div class="facets-item-cell-list-item">

		<a class="product-list-display-full-name-anchor" {{linkAttributes}}> {{productName}}</a>
		
		<div data-view="Item.SelectedOptions"></div>

		<p class="product-list-display-full-stock">
			<div data-view="ItemViews.Stock"></div>
		</p>

		<div data-view="ProductList.DetailsMinQuantity"></div>

	</div>

	<div class="facets-item-cell-list-details">
		<div class="facets-item-cell-list-description">
			{{{storeDescription}}}
		</div>
	</div>

	<div class="facets-item-cell-list-price-container">
        <div class="facets-item-cell-list-price">

			{{#if dontShowPrice}}
				<div class="facets-item-cell-list-no-price-message">{{noPriceMessage}}</div>
			{{else}}
				<div data-view="ItemViews.Price"></div>
			{{/if}}
		
		</div>
	</div>





	<div class="product-list-display-full-extras">
		<p class="product-list-display-full-quantity">
			<span class="product-list-display-full-quantity-label">{{translate 'Desired Quantity: '}}</span>
			<span class="product-list-display-full-quantity-value">{{quantity}}</span>
		</p>

		<p class="product-list-display-full-priority">
			<span class="product-list-display-full-priority-label">{{translate 'Priority: '}}</span>
			<span class="product-list-display-full-priority-value">{{priorityName}}</span>
		</p>

		{{#if showAddedOn}}
			<p class="product-list-display-full-date">
				<span class="product-list-display-full-date-label">{{translate 'Added on: '}}</span>
				<span class="product-list-display-full-date-value">{{itemCreatedDate}}</span>
			</p>
		{{/if}}

	</div>



	<div class="product-list-display-full-actions">
		{{#if showEdit}}
			<button class="product-list-display-full-edit" data-action="edit-item" data-toggle="show-in-modal">{{translate 'Edit'}}</button>
		{{/if}}
		{{#if showMoveAction}}
			<div class="product-list-display-full-move" data-type="productlist-control-move"></div>
		{{/if}}
	</div>




</div>
{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<section class="order-wizard-cartitems-module">
	<div class="order-wizard-cartitems-module-accordion-head">
		<a class="order-wizard-cartitems-module-accordion-head-toggle {{#unless showOpenedAccordion}}collapsed{{/unless}}" data-toggle="collapse" data-target="#unfulfilled-items" aria-controls="unfulfilled-items">
		{{#if itemCountGreaterThan1}}
			{{translate '$(0) Items' itemCount}}
		{{else}}
			{{translate '1 Item'}}
		{{/if}}
		<i class="order-wizard-cartitems-module-accordion-toggle-icon"></i>
		</a>
	</div>
	<div class="order-wizard-cartitems-module-accordion-body collapse {{#if showOpenedAccordion}}in{{/if}}" id="unfulfilled-items" role="tabpanel">
		<div class="order-wizard-cartitems-module-accordion-container" data-content="order-items-body">
			{{#if showEditCartButton}}
				<div class="order-wizard-cartitems-module-edit-cart-label">
					<a href="#" class="order-wizard-cartitems-module-edit-cart-link" data-action="edit-module" data-touchpoint="viewcart">
						{{translate 'Edit Cart'}}
					</a>
				</div>
			{{/if}}
			<div class="facets-facet-browse-columns">
	            <div class="facets-facet-browse-items-column">Item</div>
	            <div class="facets-facet-browse-details-column">Item Details</div>
	            <div class="facets-facet-browse-price-content-column">
	                <div class="facets-facet-browse-price-column">Price</div>
	                <div class="facets-facet-browse-qty-column">QTY</div>
	                <div class="facets-facet-browse-amount-column">Amount</div>
	            </div>
	        </div>
			<div class="order-wizard-cartitems-module-products-scroll">
				<div class="{{#if showMobile}}lg2sm-first{{/if}} order-wizard-cartitems-module-table">
					<div data-view="Items.Collection"></div>
				</div>
			</div>

		</div>
	</div>
</section>
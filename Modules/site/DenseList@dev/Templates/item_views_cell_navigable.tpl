{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}


<div class="facets-item-cell-list" itemprop="itemListElement" data-item-id="{{itemId}}" itemscope
     itemtype="http://schema.org/Product" data-track-productlist-list="{{track_productlist_list}}"
     data-track-productlist-category="{{track_productlist_category}}"
     data-track-productlist-position="{{track_productlist_position}}" data-sku="{{sku}}">
    <div class="facets-item-cell-list-item">
        {{#if isNavigable}}
			<a class="item-views-cell-navigable-product-title-anchor" {{{itemURLAttributes}}}>{{itemName}}</a>
		{{else}}
			<span class="item-views-cell-navigable-product-title">
				{{itemName}}
			</span>
		{{/if}}
    </div>
    <div class="facets-item-cell-list-details">
        <div class="facets-item-cell-list-description">
            {{{storeDescription}}}
            {{#if showOptions}}
				<div data-view="Item.Options"></div>
			{{/if}}
            {{#if dropShipItem}}
                {{{stockMessage}}}
            {{/if}}
            {{#if specialOrderItem}}
                {{{stockMessage}}}
            {{/if}}
            {{#if isBuiltToOrder}}
                {{{stockMessage}}}
            {{/if}}
        </div>
        
    </div>
    <div class="facets-item-cell-list-price-container">
        <div class="facets-item-cell-list-price">
            {{#if showBlockDetail2}}
				<p>
				{{#if showDetail2Title}}
					<span class="item-views-cell-navigable-item-unit-price-label">{{detail2Title}} </span>
				{{/if}}
				<span class="item-views-cell-navigable-item-reason-value">{{detail2}}</span>
				</p>
			{{/if}}
        </div>

    </div>
     <div class="facets-item-cell-list-qty-container">
        <div class="facets-item-cell-list-qty">
            <span class="item-views-cell-navigable-item-quantity-value">{{quantity}}</span>
        </div>

    </div>
    <div class="facets-item-cell-list-amount-container">
        <div class="facets-item-cell-list-amount">
            <p>
			{{#if showDetail3Title}}
				<span class="item-views-cell-navigable-item-amount-label">{{detail3Title}} </span>
			{{/if}}
			<span class="item-views-cell-navigable-item-amount-value">{{detail3}}</span>
			{{#if showComparePrice}}
				<small class="item-views-cell-navigable-item-old-price">{{comparePriceFormatted}}</small>
			{{/if}}
			</p>
        </div>

    </div>
</div>

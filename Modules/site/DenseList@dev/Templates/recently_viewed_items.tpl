{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}


{{#if showCells}}
    <aside class="item-relations-related">
        <h3>{{translate 'Recently viewed'}}</h3>
        <div class="facets-facet-browse-columns">
            <div class="facets-facet-browse-items-column">Item</div>
            <div class="facets-facet-browse-details-column">Item Details</div>
            <div class="facets-facet-browse-price-content-column">
                <div class="facets-facet-browse-price-column">Price</div>
                <div class="facets-facet-browse-qty-column">QTY</div>
                <div class="facets-facet-browse-stock-column">Stock Status</div>
                <div class="facets-facet-browse-addtocart-column">Add To Cart</div>
            </div>
        </div>
        <div class="item-relations-related-row">
            <div data-type="backbone.collection.view.rows"></div>
        </div>
    </aside>
{{/if}}
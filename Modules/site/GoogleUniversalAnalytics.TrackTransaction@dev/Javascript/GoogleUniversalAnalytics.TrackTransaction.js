
define('GoogleUniversalAnalytics.TrackTransaction',
[
    'GoogleUniversalAnalytics',
    'underscore',
    'SC.Configuration'
], function(GoogleUniversalAnalytics, _, Configuration) {
    'use strict';

    return _.extend(GoogleUniversalAnalytics.prototype, {
        
        trackTransaction: function(transaction) 
        {
            var transaction_id = transaction.get('confirmationNumber'); 
            GoogleUniversalAnalytics.addTrans({ 
                id: transaction_id 
            ,   revenue: transaction.get('subTotal') 
            ,   shipping: transaction.get('shippingCost') + transaction.get('handlingCost') 
            ,   tax: transaction.get('taxTotal') 
            ,   currency: SC.ENVIRONMENT.currentCurrency && SC.ENVIRONMENT.currentCurrency.code || '' 
            ,   page: '/' + Backbone.history.fragment 
            }); 
            transaction.get('products').each(function (product) 
            { 
                GoogleUniversalAnalytics.addItem({ 
                    id: transaction_id 
                ,   affiliation: Configuration.get('siteSettings.displayname') 
                ,   sku: product.get('sku') 
                ,   name: product.get('name') 
                ,   category: product.get('category') || '' 
                ,   price: product.get('rate') 
                ,   quantity: product.get('quantity') 
                }); 
            }); 
            return GoogleUniversalAnalytics.trackTrans(); 
        }
    });
});
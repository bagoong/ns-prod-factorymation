{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div id="banner-footer" class="content-banner banner-footer" data-cms-area="global_banner_footer" data-cms-area-filters="global"></div>
<div class="footer-content">
	<div class="footer-content-nav">

		<ul class="footer-content-nav-list about-us">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".about-us .footer-collapsed-list" data-type="collapse">{{translate 'About us'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/contact-us-page" data-hashtag="#contact-us-page" data-touchpoint="home">{{translate 'Contact Us'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/terms-and-conditions" data-hashtag="#terms-and-conditions" data-touchpoint="home">{{translate 'Terms and Conditions'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/returns-policy-page" data-hashtag="#returns-policy-page" data-touchpoint="home">{{translate 'Returns Policy'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/privacy-policy-page" data-hashtag="#privacy-policy-page" data-touchpoint="home">{{translate 'Privacy Policy'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/careers-page" data-hashtag="#careers-page" data-touchpoint="home">{{translate 'Careers'}}</a>
			</li>
		</ul>

		<ul class="footer-content-nav-list my-account">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".my-account .footer-collapsed-list" data-type="collapse">{{translate 'Media'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/news-room" data-hashtag="#news-room" data-touchpoint="home">{{translate 'Newsroom'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/knowledge-base" data-hashtag="#knowledge-base" data-touchpoint="home">{{translate 'Knowledge Base'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/software-downloads" data-hashtag="#software-downloads" data-touchpoint="home">{{translate 'Software Downloads'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/online-download-catalog" data-hashtag="#download-catalog" data-touchpoint="home">{{translate 'Catalog'}}</a>
			</li>
		</ul>

		<ul class="footer-content-nav-list customer-service">
			<li class="first">
				<a href="" class="first" data-toggle="collapse" data-target=".customer-service .footer-collapsed-list" data-type="collapse">{{translate 'My Account'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/purchases" data-hashtag="#purchases" data-touchpoint="customercenter">{{translate 'Order History'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/wishlist" data-hashtag="#wishlist" data-touchpoint="customercenter">{{translate 'Bill of Materials'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/free-shipping" data-hashtag="#free-shipping" data-touchpoint="home">{{translate 'Free Shipping'}}</a>
			</li>
			<li class="footer-collapsed-list">
				<a href="/NET30-setup" data-hashtag="#NET30-setup" data-touchpoint="home">{{translate 'NET30 Setup'}}</a>
			</li>
		</ul>

		<ul class="footer-content-nav-list company-name">
			<li class="first" data-toggle="collapse" data-target=".company-name .footer-collapsed-list" data-type="collapse">{{translate 'Factorymation, LLC.'}}</li>
			<li class="footer-collapsed-list">160 Bluffs Ct</li>
			<li class="footer-collapsed-list">Canton, GA 30114</li>
			<li class="footer-collapsed-list"><i class="phone-icon"></i>{{translate 'Tel: 1.800.972.0436'}}</li>
			<li class="footer-collapsed-list"><i class="envelope-icon"></i><a href="mailto:support@factorymation.com">{{translate 'Email: support@factorymation.com'}}</a></li>
		</ul>
		<ul class="footer-content-nav-list customer-service">
			<li class="first">
				<table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose Symantec SSL for secure e-commerce and confidential communications.">
					<tr>
						<td width="135" align="center" valign="top">
						<div id="global_footer_ssl" class="" data-cms-area="global_footer_ssl" data-cms-area-filters="global">
							
						</div>

						</td>
					</tr>
				</table>
			</li>
		</ul>
	</div>

</div>

<div class="footer-content-copyright">
    <ul>
        <li><a href="/about-us-page">{{translate '&copy; 2016, FactoryMation, LLC. All Rights Reserved.'}}</a></li>
    </ul>
</div>


<!-- Start of LiveChat (www.livechatinc.com) code -->

<!-- End of LiveChat code -->

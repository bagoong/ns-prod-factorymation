define('ItemViews.Option.View.Extension',[
    'ItemViews.Option.View',
    'item_views_option_dropdown.tpl',
    'underscore'
],
function (
    ItemViewsOptionView,
    item_views_option_dropdown_tpl,
    _
) {
    'use strict';
    _.extend(ItemViewsOptionView.prototype, {
        initialize: function () {
            if (this.options.tpl == 'dropdown') {
                this.template = this.model.get('type') == 'select' ? item_views_option_dropdown_tpl : this.model.get('templates').selector;
            }
            else {
                this.template = this.model.get('templates').selector;
            }
            //this.template = item_views_option_dropdown_tpl;
        }
    });
});
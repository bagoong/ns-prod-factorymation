define(
	'ItemViews.Price.View.Extension'
	, [
		'ItemViews.Price.View'
		, 'underscore'
	]
	, function (ItemViewsPriceView , _) {
		'use strict';
		_.extend(ItemViewsPriceView.prototype, {
			getContext: _.wrap(ItemViewsPriceView.prototype.getContext, function (fn) {
				var ret = fn.apply(this, _.toArray(arguments).slice(1));

				ret.dontShowPrice = this.model.get('dontshowprice');
				ret.noPriceMessage = this.model.get('nopricemessage');

				return ret;

			})

		});
	});
define('ItemViews.Cell.Actionable.View.Extend', [
    'ItemViews.Cell.Actionable.View',
    'underscore'

], function (ItemViewsCellActionableView,
             _) {

    'use strict';

    _.extend(ItemViewsCellActionableView.prototype, {
        getContext: _.wrap(ItemViewsCellActionableView.prototype.getContext, function (fn) {
            var ret = fn.apply(this, _.toArray(arguments).slice(1));
            // Adding fields to retrieve from the backend

            var item = ret.item;

            ret.obsolete = null;
    				if(item.get('outofstockbehavior') == 'Remove item when out-of-stock' && item.get('quantityavailable') - item.get('quantity') < 0) {
    					ret.obsolete = 'This item is obsolete and cannot be backordered. Available stock quantity is currently ' + item.get('quantityavailable')  + '. Please remove or reduce cart quantity so that the order may be submitted.';
    				}

            ret.showBackorderMsg = null;
            if((item.get('quantityavailable') - item.get('quantity') < 0) && item.get('outofstockbehavior') == 'Allow back orders but display out-of-stock message') {
              var backorderQty = item.get('quantityavailable');
              ret.showBackorderMsg = 'Stock quantities are insufficient for the quantity placed in the cart. Available stock quantity is currently ' + backorderQty + '. The remaining quantity will be backordered.';
            }

            var matrixParent = item.get('matrix_parent');
            if(matrixParent){
                var images = matrixParent.itemimages_detail;
                var flatImages = this.itemImageFlatten(images);


                item.attributes.outofstockmessage = matrixParent.outofstockmessage;
                item.attributes._images = flatImages;
                item.attributes._thumbnail = flatImages[0];
                //thumbnail = flatImages[0];
            }
            return ret;
        }),
        itemImageFlatten: function(images) {
            var self = this;
            if ('url' in images && 'altimagetext' in images) {
                return [images];
            }

            return _.flatten(_.map(images, function flatten(item) {
                if (_.isArray(item)) {
                    return item;
                }

                return self.itemImageFlatten(item);
            }));
        }

    });


});

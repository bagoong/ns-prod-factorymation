/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// @module Header
define('QS.Header.View', [
    'Header.View',
    'Backbone.CompositeView',
    'underscore',
    'jQuery',
    'Utils'
],
function QSHeaderView(
    HeaderView,
    BackboneCompositeView,
    _,
    jQuery
) {
    'use strict';

    HeaderView.prototype.initialize = function initialize() {
        var self = this;
        BackboneCompositeView.add(this);

        this.render = _(this.render).wrap(function wrapFn(originalRender) {
            originalRender.apply(self, Array.prototype.slice.call(arguments, 1));
            // in QS the site search is always visible, the next code is not needed anymore
            // Backbone.history.on('all', function() {
            //     self.verifyShowSiteSearch();
            // });

        });

        if (typeof CMS === 'undefined')
            {
                Backbone.Events.on('cms:load', function ()
                {
                    CMS.on('cms:rendered', function()
                    {
                        jQuery(".dropdown-submenu").each(function(){
                            if(jQuery(this).children("ul").length > 0){
                                jQuery(this).children(".header-menu-level2-anchor").addClass("has-submenu");
                            }
                        });
                        /** TODO: review how to make all dropdowns open by default and not collapsible **/
                        if(!_.isPhoneDevice()) {
                            jQuery('.side-bar-landings > .dropdown')
                                .addClass('open');
                        }
                        
                    });
                });
            }
            else
            {
                CMS.on('cms:rendered', function()
                {
                    jQuery(".dropdown-submenu").each(function(){
                        if(jQuery(this).children("ul").length > 0){
                            jQuery(this).children(".header-menu-level2-anchor").addClass("has-submenu");
                        }
                    });
                    /** TODO: review how to make all dropdowns open by default and not collapsible **/
                    /** TODO: Fix duplication! **/
                    if(!_.isPhoneDevice()) {
                        jQuery('.side-bar-landings > .dropdown')
                            .addClass('open');
                    }


                });
            }

        


    };
});

{{!
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<nav class="header-menu-secondary-nav">

	    <ul class="header-menu-level1 nav navbar-nav">
        <!--
        {{#each categories}}
        <li class="dropdown">
            <a class="{{class}}" {{objectToAtrributes this}} {{#if categories}}data-toggle="dropdown"{{/if}}>
            {{text}}
            <i class="arrow-down"></i>
            </a>
            {{#if categories}}

                <ul class="header-menu-level2 dropdown-menu">
                    {{#each categories}}
                    <li class="dropdown dropdown-submenu">
                        <a class="{{class}}" {{#if categories}}{{/if}} {{objectToAtrributes this}}>{{text}}</a>

                        {{#if categories}}
                        <ul class="header-menu-level3 dropdown-menu">
                            {{#each categories}}
                            <li>
                                <a class="{{class}}" {{objectToAtrributes this}}>{{text}}</a>
                            </li>
                            {{/each}}
                        </ul>
                        {{/if}}
                    </li>
                    {{/each}}
                </ul>

            {{/if}}
        </li>
        {{/each}}
        -->

        <li class="cms-custom-main-menu" data-cms-area="cms_custom_media_menu" data-cms-area-filters="global">&nbsp;</li>
        <!--li class="cms-custom-support-menu" data-cms-area="cms_custom_support_menu" data-cms-area-filters="global">&nbsp;</li>
        <li class="cms-custom-company-menu" data-cms-area="cms_custom_company_menu" data-cms-area-filters="global">&nbsp;</li-->
        <li class="cms-custom-phone-number" data-cms-area="cms_custom_phone_number" data-cms-area-filters="global">&nbsp;</li>

    </ul>


</nav>

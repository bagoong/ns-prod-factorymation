{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="home">

    <!--div class="home-banner-top">
        <p class="home-banner-top-message">
            {{translate 'Use promo code <strong>SCADEMO</strong> ffor <strong>30%</strong> off your purchase'}}
        </p>
    </div -->
    <div class="home-slider-container" data-cms-area="home-slider-container" data-cms-area-filters="global">


    </div>

    <div id="merchandising-zone-message" class="merchandising-zone-message" data-cms-area="merchandising-zone-message" data-cms-area-filters="page_type"></div>
    <div id="merchandising-zone" class="merchandising-zone home-merchandising-zone" data-cms-area="merchandising-zone" data-cms-area-filters="page_type"></div>

    <div class="home-banner-main">
        <div class="home-banner-main-cell-nth1">
            <div data-view="QuickOrder"></div>
        </div>

        <div class="home-banner-video">
            <h1>
                {{translate 'Product Videos'}}
            </h1>
            <a href="https://www.youtube.com/user/FactoryMation/playlists" target="_blank">
                <img src="{{thumbVideo}}" class="thumb-video-image">
            </a>
            <p>
                {{translate 'Checkout our video library on YouTube for highlights on our featured products'}}
            </p>
        </div>

        <div class="banner-mini-breakers" data-cms-area="home_breakers" data-cms-area-filters="path">
            <!--<h1>
                Fmx Motor Controls
            </h1>
            <div class="banner-breakers">
                <span>
                    <img src="/../app/img/mini-breakers.jpg" class="mini-breakers">
                </span>
                <h3>FMX Mini Breakers</h3>
            </div>
            <a href="http://www.factorymation.com/Products/Mini_Circuit_Breakers/" target="_blank" style="margin-top:40px">
                    More Info
            </a>-->
        </div>

    </div>
    <div class="home-merchandizing-zone">
        <div data-id="your-merchandising-zone" data-type="merchandising-zone"></div>
    </div>

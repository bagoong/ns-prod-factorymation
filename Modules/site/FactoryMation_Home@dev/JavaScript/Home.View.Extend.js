define('Home.View.Extend', [
    'Home.View',
    'underscore'
],
function HomeViewExt(
    HomeView,

    _
) {
    'use strict';

     HomeView.prototype.installPlugin('postContext', {
        name: 'HomeViewExt',
        priority: 10,
        execute: function execute(context, view) {

            _.extend(context, {
                thumbVideo: '/../app/img/VideoPromo.jpg',
                bannerMiniBreakers: '/../app/img/mini-breakers.jpg'
            });
        }
    });
});

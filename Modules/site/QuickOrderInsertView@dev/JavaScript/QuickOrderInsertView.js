define('QuickOrderInsertView', [
    'Facets.FacetedNavigation.View',
    'Home.View',
    'QuickOrder.View',
    'Backbone.CompositeView',
    'underscore'
], function NewsletterSignUp(FacetsFacetedNavigationView,
                             HomeView,
                             QuickOrderView,
                             BackboneCompositeView,
                             _) {
    'use strict';

    if(!SC.isPageGenerator()) {
        _.extend(FacetsFacetedNavigationView.prototype.childViews, {
            'QuickOrder': function QuickOrderViewFn() {
                return new QuickOrderView();
            }
        });

        _.extend(HomeView.prototype, {
            initialize: _.wrap(HomeView.prototype.initialize, function (fn) {
                BackboneCompositeView.add(this);
                fn.apply(this, _.toArray(arguments).slice(1));
            })
        });

        _.extend(HomeView.prototype.childViews, {
            'QuickOrder': function QuickOrderViewFn() {
                return new QuickOrderView();
            }
        });
    }
});

define(
    'ItemDetailsTabExtension.View'
    , [
        'ItemDetails.View'
        , 'underscore'
        , 'jQuery'
    ]
    , function (ItemDetailsView
        , _, jQuery) {
        'use strict';
        _.extend(ItemDetailsView.prototype, {
            computeDetailsArea: _.wrap(ItemDetailsView.prototype.computeDetailsArea, function (fn) {
                fn.apply(this, _.toArray(arguments).slice(1));
                var countTab = this.details.length;
                var content = jQuery.trim('<div></div>');
                var toPush = {
                    name: 'Related Items'
                    , isOpened: false
                    , content: content
                    , itemprop: 'Related Items'
                };
                if(countTab == 0){
                    this.details.push(toPush);
                }else{
                    this.details.splice(1, 0, toPush);
                }

            }),
            render: _.wrap(ItemDetailsView.prototype.render, function (fn) {
                fn.apply(this, _.toArray(arguments).slice(1));
                this.$('#item-details-content-container-1').append(this.$('.item-details-content-related-items').remove());
            }),

            getContext: _.wrap(ItemDetailsView.prototype.getContext, function (fn) {
                var item = this.model;
                var details_object = item.get('_priceDetails') || {}
                    , hasPriceSchedule = false
                    , priceSchedule = [];

                if (details_object.priceschedule && details_object.priceschedule.length) {
                    hasPriceSchedule = true;

                    var price, min;

                    for (var i = 0; i < details_object.priceschedule.length; i++) {
                        price = details_object.priceschedule[i];
                        min = parseInt(price.minimumquantity, 10);
                        min = min == 0 ? 1 : min;
                        priceSchedule.push({
                            price: price.price_formatted,
                            minimunQty: min
                        });
                    }
                }

                var result = _.extend(fn.apply(this, _.toArray(arguments).slice(1)), {
                    hasPriceSchedule: hasPriceSchedule,
                    priceSchedule: priceSchedule
                });

                return result;

            })
        });

    });
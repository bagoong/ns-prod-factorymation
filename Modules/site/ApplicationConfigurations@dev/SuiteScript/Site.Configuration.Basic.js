define('Site.Configuration', [
    'Configuration',
    'PSCategories.Configuration'
], function SiteConfiguration(
    Configuration,
    CategoriesConfiguration
) {
    'use strict';

    CategoriesConfiguration.secureCategories = true;
    SC.Configuration.filter_site =  'all';

});
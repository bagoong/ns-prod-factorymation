define('Site.Configuration', [
    'Configuration',
    'PSCategories.Configuration',
    'BackInStockNotification.Configuration',
    'GuestOrderStatus.Configuration',
    'HashtagGallery.Configuration',
    'StoreLocator.Configuration'

], function SiteConfiguration(
    Configuration,
    CategoriesConfiguration,
    BackInStockNotificationConfiguration,
    GuestOrderStatusConfiguration,
    HashtagGalleryConfiguration,
    StoreLocatorConfiguration
) {
    'use strict';

    CategoriesConfiguration;
    BackInStockNotificationConfiguration;
    GuestOrderStatusConfiguration;
    HashtagGalleryConfiguration;
    StoreLocatorConfiguration;


    /*
    Configuration.webFonts = {
        enabled: true,
        async: true,
        configuration: {
            google: { families: [ 'Roboto:400,700,300:latin' ] }
        }
    };
    */

    // IMPORTANT THINGS TO CUSTOMIZE
    StoreLocatorConfiguration.googleMapsApiKey = 'ABCD';


    CategoriesConfiguration.secureCategories = true;

});
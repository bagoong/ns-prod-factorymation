define('Site.Global.Configuration', [
    'SC.Configuration',

    'item_views_option_color.tpl',
    'item_views_selected_option_color.tpl',

    'item_views_option_dropdown.tpl',
    'item_views_option_text.tpl',
    'item_views_selected_option.tpl',

    'underscore'
], function SiteGlobalConfiguration(Configuration,
                                    itemViewsOptionColorTemplate,
                                    itemViewsSelectedOptionColorTemplate,
                                    itemViewsOptionDropDownTemplate,
                                    itemViewsOptionTextTemplate,
                                    itemViewsSelectedOptionTemplate) {
    'use strict';

    var colors = {
        'black': '#212121',
        'Black': '#212121',
        'gray': '#9c9c9c',
        'Gray': '#9c9c9c',
        'grey': '#9c9c9c',
        'Grey': '#9c9c9c',
        'white': '#ffffff',
        'White': '#ffffff',
        'brown': '#804d3b',
        'Brown': '#804d3b',
        'beige': '#eedcbe',
        'Beige': '#eedcbe',
        'blue': '#0f5da3',
        'Blue': '#0f5da3',
        'light-blue': '#8fdeec',
        'Light-blue': '#8fdeec',
        'purple': '#9b4a97 ',
        'Purple': '#9b4a97 ',
        'lilac': '#ceadd0',
        'Lilac': '#ceadd0',
        'red': '#f63440',
        'Red': '#f63440',
        'pink': '#ffa5c1',
        'Pink': '#ffa5c1',
        'orange': '#ff5200',
        'Orange': '#ff5200',
        'peach': '#ffcc8c',
        'Peach': '#ffcc8c',
        'yellow': '#ffde00',
        'Yellow': '#ffde00',
        'light-yellow': '#ffee7a',
        'Light-yellow': '#ffee7a',
        'green': '#00af43',
        'Green': '#00af43',
        'lime': '#c3d600',
        'Lime': '#c3d600',
        'teal': '#00ab95',
        'Teal': '#00ab95',
        'aqua': '#28e1c5',
        'Aqua': '#28e1c5',
        'burgandy': '#9c0633',
        'Burgandy': '#9c0633',
        'navy': '#002d5d',
        'Navy': '#002d5d',
        'cyan': '#66FFFF',
        'Cyan': '#66FFFF',
        'multi': '#111111',
        'Multi': '#111111'
        ,'amber': '#FFC200'
        ,'Amber': '#FFC200'
    };
    /* Add your global config here. It will be merged in the specific app config, not here */


    return {
        colors: colors,

        //USE of AdvancedItemImagesModule
        advancedImageManagement: true,
        multiImageOption: ['custcol1'],
        showOnlyMainImages: true,

        // Use StoreEmail cookie to suggest email on login
        rememberEmailOnLoginPage: false,

        navigationData: [
            {text: _('Products').translate(),
            href: '/search',
            'class': 'header-menu-shop-anchor',
            data: {
                touchpoint: 'home',
                hashtag: '#/search'
            }
        }/*, {
            text: _('Media').translate(),
            href: '/media',
            'class': 'header-menu-shop-anchor',
            data: {
                touchpoint: 'home',
                hashtag: '#/media'
            },
            categories: [
                {
                    text: _('News Room').translate(),
                    href: '/news-room',
                    'class': 'header-menu-level1-anchor',
                    data: {
                        touchpoint: 'home',
                        hashtag: '#/news-room'
                    }
                },
                {
                    text: _('Product Videos').translate(),
                    href: '/product-videos',
                    'class': 'header-menu-level1-anchor',
                    data: {
                        touchpoint: 'home',
                        hashtag: '#/product-videos'
                    }
                },
                {
                    text: _('Online/Download Catalog').translate(),
                    href: '/online-download-catalog',
                    'class': 'header-menu-level1-anchor',
                    data: {
                        touchpoint: 'home',
                        hashtag: '#/online-download-catalog'
                    }
                },
                {
                    text: _('Request a Catalog').translate(),
                    href: '/request-a-catalog',
                    'class': 'header-menu-level1-anchor',
                    data: {
                        touchpoint: 'home',
                        hashtag: '#/request-a-catalog'
                    }
                }
            ]
        }, {
            text: _('Support').translate(),
            href: '/support',
            'class': 'header-menu-shop-anchor',
            data: {
                touchpoint: 'home',
                hashtag: '#/support'
            },
            categories: [
                {
                    text: _('Contact us').translate(),
                    href: '/contact-us',
                    'class': 'header-menu-level1-anchor',
                    data: {
                        touchpoint: 'home',
                        hashtag: '#/contact-us'
                    }
                },
                {
                    text: _('Software Downloads').translate(),
                    href: '/software-downloads',
                    'class': 'header-menu-level1-anchor',
                    data: {
                        touchpoint: 'home',
                        hashtag: '#/software-downloads'
                    }
                },
                {
                    text: _('Knowledge Base').translate(),
                    href: '/knowledge-base',
                    'class': 'header-menu-level1-anchor',
                    data: {
                        touchpoint: 'home',
                        hashtag: '#/knowledge-base'
                    }
                },
                {
                    text: _('NET30 Setup').translate(),
                    href: '/NET30-setup',
                    'class': 'header-menu-level1-anchor',
                    data: {
                        touchpoint: 'home',
                        hashtag: '#/NET30-setup'
                    }
                }
            ]
        }
            , {
                text: _('Company').translate(),
                //href: '/about-us',
                'class': 'header-menu-shop-anchor',
               
                categories: [
                    {
                        text: _('About us').translate(),
                       href: '/about-us-page',
                        'class': 'header-menu-level1-anchor',
                        data: {
                            touchpoint: 'home',
                            hashtag: '#/about-us-page'
                        }
                    },
                    {
                        text: _('Contact us').translate(),
                        href: '/contact-us-page',
                        'class': 'header-menu-level1-anchor',
                        data: {
                            touchpoint: 'home',
                            hashtag: '#/contact-us-page'
                        }
                    },
                    {
                        text: _('Free Shipping').translate(),
                        href: '/free-shipping',
                        'class': 'header-menu-level1-anchor',
                        data: {
                            touchpoint: 'home',
                            hashtag: '#/free-shipping'
                        }
                    },
                    {
                        text: _('NET30 Setup').translate(),
                        href: '/NET30-setup',
                        'class': 'header-menu-level1-anchor',
                        data: {
                            touchpoint: 'home',
                            hashtag: '#/NET30-setup'
                        }
                    },
                    {
                        text: _('Terms and Conditions').translate(),
                        href: '/terms-and-conditions',
                        'class': 'header-menu-level1-anchor',
                        data: {
                            touchpoint: 'home',
                            hashtag: '#/terms-and-conditions'
                        }
                    },
                    {
                        text: _('Returns Policy').translate(),
                        href: '/returns-policy-page',
                        'class': 'header-menu-level1-anchor',
                        data: {
                            touchpoint: 'home',
                            hashtag: '#/returns-policy-page'
                        }
                    },
                    {
                        text: _('Privacy Policy').translate(),
                        href: '/privacy-policy',
                        'class': 'header-menu-level1-anchor',
                        data: {
                            touchpoint: 'home',
                            hashtag: '#/privacy-policy'
                        }
                    },
                    {
                        text: _('Careers').translate(),
                        href: '/careers',
                        'class': 'header-menu-level1-anchor',
                        data: {
                            touchpoint: 'home',
                            hashtag: '#/careers'

                        }
                    }
                ]
            }*/]
        , templates: {
            itemOptions: {
                // each apply to specific item option types
                selectorByType:	{
                    select: itemViewsOptionDropDownTemplate
                        ,	'default': itemViewsOptionTextTemplate
                }
                // for rendering selected options in the shopping cart
            ,	selectedByType: {
                    'default': itemViewsSelectedOptionTemplate
                }
            }
        }
        ,
        itemOptions: [{
            // update the name of the custcol here in order to get this working correctly
            cartOptionId: 'custcol1',
            label: 'Color',
            url: 'color',
            colors: colors,
            showSelectorInList: true,
            templates: {
                    selector: itemViewsOptionColorTemplate,
                    // 'shoppingCartOptionColor'
                    selected: itemViewsSelectedOptionColorTemplate,
                }
            }
            ,{
            cartOptionId: 'custcol42',
            label: 'Test List',
            url: 'testColURL',
            showSelectorInList: true,
            templates: {
                selector: itemViewsOptionDropDownTemplate,
                selected: itemViewsSelectedOptionTemplate
            }
        }],
        extraModulesConfig: {
            Categories: {
                addToNavigationTabs: true,
                classLevels: [
                    'header-menu-level1-anchor',
                    'header-menu-level2-anchor',
                    'header-menu-level3-anchor'
                ],
                method: 'child',
                mergeIndex: 1,
                categoryOnProductURL: false,
                levelsOnMenu: 3
            }
        }
        ,	tracking: {
            // [Google Universal Analytics](https://developers.google.com/analytics/devguides/collection/analyticsjs/)
            googleUniversalAnalytics: {
                propertyID: 'UA-30169505-1'
                ,	domainName: window.location.hostname
            }
            // [Google AdWords](https://support.google.com/adwords/answer/1722054/)
            ,	googleAdWordsConversion: {
                id: 1070532133
                ,	value: 0
                ,	label: 'YyORCK6si2sQpYy8_gM'
            }
        }
    };
});
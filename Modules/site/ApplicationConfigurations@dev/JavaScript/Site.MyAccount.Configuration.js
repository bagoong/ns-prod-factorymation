define('Site.MyAccount.Configuration', [
    'SC.MyAccount.Configuration',
    'Site.Global.Configuration',
    'underscore'
], function SiteCheckoutConfiguration(
    MyAccountConfiguration,
    GlobalConfiguration,
    _
) {
    'use strict';

    var SiteApplicationConfiguration = {

      addToCartFromFacetsView: true

      // @property {String} addToCartBehavior Defines what happens after the user adds an item to the cart.
    	// Available values are: goToCart, showMiniCart or showCartConfirmationModal
    ,	addToCartBehavior: 'showCartConfirmationModal'

    };

    var extraModulesConfig = GlobalConfiguration.extraModulesConfig;
    delete GlobalConfiguration.extraModulesConfig;

    MyAccountConfiguration.modulesConfig = MyAccountConfiguration.modulesConfig || {};
    _.extend(MyAccountConfiguration, GlobalConfiguration, SiteApplicationConfiguration);
    _.extend(MyAccountConfiguration.modulesConfig, extraModulesConfig);

    return {
        mountToApp: function mountToApp(application) {
            _.extend(application.Configuration, MyAccountConfiguration);
        }
    };
});

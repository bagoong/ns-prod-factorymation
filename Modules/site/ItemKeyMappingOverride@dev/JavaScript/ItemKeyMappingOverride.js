define('ItemKeyMappingOverride', [
    'ItemsKeyMapping',
    'underscore',
    'SC.Configuration'
], function ItemImages(ItemsKeyMapping,
                       _,
                       Configuration) {
    'use strict';

    function extendKeyMapping() {
        Configuration.itemKeyMapping = Configuration.itemKeyMapping || {};

        _.extend(Configuration.itemKeyMapping, {
            _optionsDetails: function (item) {
                var originOptions = item.get('itemoptions_detail');
                if (originOptions) {
                    originOptions.fields = _.reject(originOptions.fields, function (field) {
                        return field.internalid == 'custcol_nswmspackcode' || field.internalid == 'custcol_create_fulfillment_order';
                    });
                }
                return originOptions;
            }
        });
    }

    return {
        mountToApp: function mountToApp(application) {
            extendKeyMapping();
        }
    };
});
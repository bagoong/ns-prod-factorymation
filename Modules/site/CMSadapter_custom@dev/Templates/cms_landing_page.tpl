{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="cms-landing-page-row"> 
	<div class="col-md-3 col-sm-3" data-cms-area="cms-main-menu-left" data-cms-area-filters="path"></div>
	<div class="col-md-9 col-sm-9" data-cms-area="cms-landing-page-placeholder-path" data-cms-area-filters="path"></div>
</div>

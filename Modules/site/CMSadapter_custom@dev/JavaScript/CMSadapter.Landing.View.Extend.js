define(
    'CMSadapter.Landing.View.Extend'
    , [
        'CMSadapter.Landing.View'
        , 'underscore'
        , 'jQuery'
        , 'Utils'
    ]
    , function (CMSadapterLandingView
        , _, jQuery, Utils) {
        'use strict';
        _.extend(CMSadapterLandingView.prototype, {
            initialize: _.wrap(CMSadapterLandingView.prototype.initialize, function (fn) {
                if (typeof CMS === 'undefined')
                    {
                        Backbone.Events.on('cms:load', function ()
                        {
                            CMS.on('cms:rendered', function()
                            {
                                if ( jQuery('.cms-landing-page-row > .col-md-3 > .cms-content ul').length == 0){
                                    jQuery('#cms-landing-page').addClass('no-sidebar')
                                }
                                
                            });
                        });
                    }
                    else
                    {
                        CMS.on('cms:rendered', function()
                        {
                            if ( jQuery('.cms-landing-page-row > .col-md-3 > .cms-content ul').length == 0){
                                jQuery('#cms-landing-page').addClass('no-sidebar')
                            }
                       
                        });
                    }

            })

        });
    });
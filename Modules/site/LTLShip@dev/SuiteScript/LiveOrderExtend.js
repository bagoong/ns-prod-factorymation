define('LiveOrderExtend',[
    'LiveOrder.Model',
    'StoreItem.Model',
    'Utils',
    'underscore'
], function (LiveOrderModel, StoreItem, Utils, _) {
    'use strict';

    var config = {
        LTLShipings: {
            commercialNoExtras: "56182"
            , commercialWithLgate: "131323"
            , commercialWithNofify: "131324"
            , commercialWithLgateAndNotify: "131325"
            , residentialNoExtras: "56181"
            , residentialWithLgate: "131327"
        }
        , minWeight: 249
    };
    _.extend(LiveOrderModel, {
        get: function () {
            nlapiLogExecution('DEBUG', 'get ext', 'get ext');
            var order_fields = this.getFieldValues()
                , result = {};

            // @class LiveOrder.Model.Data object containing high level shopping order object information. Serializeble to JSON and this is the object that the .ss service will serve and so it will poblate front end Model objects
            try {
                //@property {Array<LiveOrder.Model.Line>} lines
                result.lines = this.getLines(order_fields);
            }
            catch (e) {
                if (e.code === 'ERR_CHK_ITEM_NOT_FOUND') {
                    return this.get();
                }
                else {
                    throw e;
                }
            }

            order_fields = this.hidePaymentPageWhenNoBalance(order_fields);

            // @property {Array<String>} lines_sort sorted lines ids
            result.lines_sort = this.getLinesSort();

            // @property {String} latest_addition
            result.latest_addition = context.getSessionObject('latest_addition');

            // @property {LiveOrder.Model.PromoCode} promocode
            result.promocode = this.getPromoCode(order_fields);

            // @property {Boolean} ismultishipto
            result.ismultishipto = this.getIsMultiShipTo(order_fields);

            // Ship Methods
            if (result.ismultishipto) {
                // @property {Array<OrderShipMethod>} multishipmethods
                result.multishipmethods = this.getMultiShipMethods(result.lines);

                // These are set so it is compatible with non multiple shipping.
                result.shipmethods = [];
                result.shipmethod = null;

                //Correct promocodes
                if (result.promocode && result.promocode.code) {
                    order.removePromotionCode(result.promocode.code);
                    return this.get(); //Recursive, as it might impact the summary information
                }

            }
            else {
                // @property {Array<OrderShipMethod>} shipmethods
                result.shipmethods = this.getShipMethods(order_fields, result.lines);
                // @property {OrderShipMethod} shipmethod
                result.shipmethod = order_fields.shipmethod ? order_fields.shipmethod.shipmethod : null;
            }

            // Addresses
            result.addresses = this.getAddresses(order_fields);
            result.billaddress = order_fields.billaddress ? order_fields.billaddress.internalid : null;
            result.shipaddress = !result.ismultishipto ? order_fields.shipaddress.internalid : null;

            // @property {Array<ShoppingSession.PaymentMethod>} paymentmethods Payments
            result.paymentmethods = this.getPaymentMethods(order_fields);

            // @property {Boolean} isPaypalComplete Paypal complete
            result.isPaypalComplete = context.getSessionObject('paypal_complete') === 'T';

            // @property {Array<String>} touchpoints Some actions in the live order may change the URL of the checkout so to be sure we re send all the touchpoints
            result.touchpoints = session.getSiteSettings(['touchpoints']).touchpoints;

            // @property {Boolean} agreetermcondition Terms And Conditions
            result.agreetermcondition = order_fields.agreetermcondition === 'T';

            // @property {OrderSummary} Summary
            result.summary = order_fields.summary;

            // @property {Object} options Transaction Body Field
            result.options = this.getTransactionBodyField();

            // @class LiveOrder.Model
            return result;
        }
        , getShipMethods: function (order_fields, lines) {
            var shipmethods = _.map(order_fields.shipmethods, function (shipmethod) {
                var rate = Utils.toCurrency(shipmethod.rate.replace(/^\D+/g, '')) || 0;

                return {
                    internalid: shipmethod.shipmethod
                    , name: shipmethod.name
                    , shipcarrier: shipmethod.shipcarrier
                    , rate: rate
                    , rate_formatted: shipmethod.rate
                };
            });
            nlapiLogExecution('DEBUG','original ships',JSON.stringify(shipmethods));

            var mustLTL = (_.filter(lines, function (line) {
                        return line.mustLTL
                    })).length > 0,
                totalWeight = _.reduce(lines, function (partialTotal, line) {
                    return partialTotal + (line.quantity * line.weight)
                }, 0),
                LTLShipMethodsIds = _.values(config.LTLShipings);

            nlapiLogExecution('DEBUG','MUST LTL',mustLTL);
            nlapiLogExecution('DEBUG','totalWeight',totalWeight);
            nlapiLogExecution('DEBUG','LTLShipMethodsIds',LTLShipMethodsIds);


            if (mustLTL) {
                nlapiLogExecution('DEBUG','MUST LTL','IN');
                shipmethods = _.filter(shipmethods, function (shipmethod) {
                    return _.contains(LTLShipMethodsIds, shipmethod.internalid);
                });
            } else if (totalWeight < config.minWeight) {
                nlapiLogExecution('DEBUG','totalWeight < config.minWeight','IN');
                shipmethods = _.reject(shipmethods, function (shipmethod) {
                    return _.contains(LTLShipMethodsIds, shipmethod.internalid);
                });
            }
            nlapiLogExecution('DEBUG','final ships',JSON.stringify(shipmethods));
            shipmethods = _.sortBy(shipmethods, function (shipMeth) {
                var pos = _.indexOf(LTLShipMethodsIds, shipMeth.internalid);
                pos = pos == -1 ? shipMeth.internalid : pos;
                return pos;
            });

            nlapiLogExecution('DEBUG','final order ships',JSON.stringify(shipmethods));

            return shipmethods;
        }
        , getLines: function (order_fields) {
            var lines = [];
            if (order_fields.items && order_fields.items.length) {
                var self = this
                    , items_to_preload = []
                    , address_book = session.isLoggedIn2() && this.isSecure ? customer.getAddressBook() : []
                    , item_ids_to_clean = [];

                address_book = _.object(_.pluck(address_book, 'internalid'), address_book);

                _.each(order_fields.items, function (original_line) {
                    // Total may be 0
                    var total = (original_line.promotionamount) ? Utils.toCurrency(original_line.promotionamount) : Utils.toCurrency(original_line.amount)
                        , discount = Utils.toCurrency(original_line.promotiondiscount) || 0
                        , line_to_add;

                    // @class LiveOrder.Model.Line represents a line in the LiveOrder
                    line_to_add = {
                        // @property {String} internalid
                        internalid: original_line.orderitemid
                        // @property {Number} quantity
                        , quantity: original_line.quantity
                        // @property {Number} rate
                        , rate: parseFloat(original_line.rate)
                        // @property {String} rate_formatted
                        , rate_formatted: original_line.rate_formatted
                        // @property {Number} amount
                        , amount: Utils.toCurrency(original_line.amount)
                        // @property {Number} tax_amount
                        , tax_amount: 0
                        // @property {Number} tax_rate
                        , tax_rate: null
                        // @property {String} tax_rate
                        , tax_code: null
                        // @property {Number} discount
                        , discount: discount
                        // @property {Number} total
                        , total: total
                        // @property {String} item internal id of the line's item
                        , item: original_line.internalid
                        // @property {String} itemtype
                        , itemtype: original_line.itemtype
                        // @property {Object} options
                        , options: original_line.options
                        // @property {OrderAddress} shipaddress
                        , shipaddress: original_line.shipaddress
                        // @property {OrderShipMethod} shipmethod
                        , shipmethod: original_line.shipmethod
                        , weight: original_line.custitem36
                        , mustLTL: original_line.custitem45
                    };
                    // @class LiveOrder.Model

                    lines.push(line_to_add);

                    if (line_to_add.shipaddress && !address_book[line_to_add.shipaddress]) {
                        line_to_add.shipaddress = null;
                        line_to_add.shipmethod = null;
                        item_ids_to_clean.push(line_to_add.internalid);
                    }
                    else {
                        items_to_preload.push({
                            id: original_line.internalid
                            , type: original_line.itemtype
                        });
                    }
                });

                if (item_ids_to_clean.length) {
                    order.setItemShippingAddress(item_ids_to_clean, null);
                    order.setItemShippingMethod(item_ids_to_clean, null);
                }

                var restart = false;

                StoreItem.preloadItems(items_to_preload);

                lines.forEach(function (line) {
                    line.item = StoreItem.get(line.item, line.itemtype);

                    if (!line.item) {
                        self.removeLine(line.internalid);
                        restart = true;
                    }
                    else {
                        line.rate_formatted = Utils.formatCurrency(line.rate);
                        line.amount_formatted = Utils.formatCurrency(line.amount);
                        line.tax_amount_formatted = Utils.formatCurrency(line.tax_amount);
                        line.discount_formatted = Utils.formatCurrency(line.discount);
                        line.total_formatted = Utils.formatCurrency(line.total);
                    }
                });

                if (restart) {
                    throw {code: 'ERR_CHK_ITEM_NOT_FOUND'};
                }

                // Sort the items in the order they were added, this is because the update operation alters the order
                var lines_sort = this.getLinesSort();

                if (lines_sort.length) {
                    lines = _.sortBy(lines, function (line) {
                        return _.indexOf(lines_sort, line.internalid);
                    });
                }
                else {
                    this.setLinesSort(_.pluck(lines, 'internalid'));
                }
            }

            return lines;
        }
    });

});

{{!
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
}}

<div class="order-wizard-shipmethod-module">
	{{#if showTitle}}
		<h3 class="order-wizard-shipmethod-module-title">
			{{title}}
		</h3>
	{{/if}}
	
	{{#if showEnterShippingAddressFirst}}
		<div class="order-wizard-shipmethod-module-message">
			{{translate 'Warning: Please enter a valid shipping address first'}}
		</div>
	{{else}}
		{{#if showLoadingMethods}}
			<div class="order-wizard-shipmethod-module-message">
				{{translate 'Loading...'}}
			</div>
		{{else}}
			{{#if hasShippingMethods}}
                {{#if hasLTL}}
                    <div class="main-delivery-option">
                        <a data-action="select-delivery-option-radio"
                           class="order-wizard-shipmethod-module-option {{#if LTLSelected}}order-wizard-shipmethod-module-option-active{{/if}}"
                           data-value="{{LTLShipings.commercialNoExtras}}" data-isLTL="T">
                            <input type="radio" name="delivery-options" data-action="edit-module" class="order-wizard-shipmethod-module-checkbox"
                                   {{#if LTLSelected}}checked{{/if}}
                                   value="{{LTLShipings.commercialNoExtras}}"
                                   id="delivery-options-{{LTLShipings.commercialNoExtras}}" data-isLTL="T"/>

                            <span class="order-wizard-shipmethod-module-option-name">{{translate 'LTL Freight Shipping'}}
                                - <span class="order-wizard-shipmethod-module-option-price">{{LTLPrice}}</span>
                            </span>
                        </a>
                        <input data-action="select-delivery-option-radio" id="check-option-residential" data-isLTL="T" type="checkbox" name="residential" value="residential" {{#if residentialSelected}}checked{{/if}} {{#if notifySelected}}disabled{{/if}}> Residential Address<br>
                        <input data-action="select-delivery-option-radio" id="check-option-notify" data-isLTL="T" type="checkbox" name="notify" value="notify" {{#if notifySelected}}checked{{/if}} {{#if residentialSelected}}disabled{{/if}}> Notify<br>
                        <input data-action="select-delivery-option-radio" id="check-option-gate" data-isLTL="T" type="checkbox" name="lift-gate" value="lift-gate" {{#if liftSelected}}checked{{/if}}> Lift Gate.
                    </div>
                {{/if}}
                {{#each shippingMethods}}
                    <a data-action="select-delivery-option-radio"
                    class="order-wizard-shipmethod-module-option {{#if isActive}}order-wizard-shipmethod-module-option-active{{/if}}"
                    data-value="{{internalid}}">
                        <input type="radio" name="delivery-options" data-action="edit-module" class="order-wizard-shipmethod-module-checkbox"
                        {{#if isActive}}checked{{/if}}
                        value="{{internalid}}"
                        id="delivery-options-{{internalid}}" />

                        <span class="order-wizard-shipmethod-module-option-name">{{name}}
                            - <span class="order-wizard-shipmethod-module-option-price">{{rate_formatted}}</span>
                        </span>
                    </a>
                {{/each}}
			{{else}}
				<div class="order-wizard-shipmethod-module-message">
					{{translate 'Warning: No Delivery Methods are available for this address'}}
				</div>
			{{/if}}
		{{/if}}
	{{/if}}
</div>
{{#if LTLSelected}}
    <div class="order-wizard-shipmethod-module">
        <h3>Terms & conditions</h3>
        <p>
            LTL shipments are 'prepaid and add' transactions due at the time of invoice. Commercial locations without dock access, residential locations, or other limited access locations (as determined by the freight company) may incur additional fees for delivery. Additional services, such as Inside Delivery, Lift gate, or Notification Prior to Delivery, and/or Residential delivery surcharge, may be required and subject to additional cost. All fees are the responsibility of the buyer and by accepting these services you authorize FactoryMation to bill you for any additional charges. All LTL shipments must be carefully inspected for damage at the time of delivery. Damaged freight should be refused and thorough notes made on the Bill of Lading. Contact FactoryMation after refusal to report the damaged shipment. Signing for acceptance of an LTL shipment acknowledges that the goods were received in good working order. Once signed for, any claims for damages must be made directly to the carrier.
        </p><br>
        <input id="acceptTermsLTL" type="checkbox" name="acceptTerms" value="acceptTerms"> I Agree to the Terms and Conditions of LTL Truck Freight
    </div>
{{/if}}
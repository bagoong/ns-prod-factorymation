/*
	© 2016 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ReorderItems
define(
	'ReorderItems.List.View.Ext'
,	[
		'ReorderItems.List.View'
	,	'LiveOrder.Model'
	,	'Backbone'
	,	'underscore'
	,	'jQuery'
	]
,	function (
		View
	,	LiveOrderModel
	,	Backbone
	,	_
	,	jQuery
	)
{
	'use strict';

	//@class ReorderItems.List.View.Ext @extends Backbone.View -> ReorderItems.List.View
	return _.extend(View.prototype, {

		addToCart: function(e) {
				e.preventDefault();

				var	self = this
				,	line_id = this.$(e.target).data('line-id')
				,	$form = this.$(e.target).closest('[itemprop="itemListElement"]')
				,	$alert_placeholder = $form.find('[data-type=alert-placeholder]')
				,	$quantity = $form.find('input[name=item_quantity]')
				,	selected_line = this.collection.get(line_id)
				,	item_to_cart = selected_line.get('item')
				,	quantity = isNaN(parseInt($quantity.val(), 10)) ? 0 : parseInt($quantity.val(), 10);

				if (quantity) {
					item_to_cart.set('quantity', quantity);
					var	cart = LiveOrderModel.getInstance()
					,	layout = this.application.getLayout()
					,	cart_promise
					,	error_message = _('Sorry, there is a problem with this Item and can not be purchased at this time. Please check back later.').translate()
					,	isOptimistic = this.application.getConfig('addToCartBehavior') === 'showCartConfirmationModal';

					if (item_to_cart.itemOptions && item_to_cart.itemOptions.GIFTCERTRECIPIENTEMAIL)
					{
						if (!Backbone.Validation.patterns.email.test(item_to_cart.itemOptions.GIFTCERTRECIPIENTEMAIL.label))
						{
							self.showError(_('Recipient email is invalid').translate());
							return;
						}
					}

					if (isOptimistic)
					{
						//Set the cart to use optimistic when using add to cart
						// TODO pass a param to the add to cart instead of using this hack!
						cart.optimistic = {
							item: item_to_cart
						,	quantity: quantity
						};
					}

					// If the item is already in the cart
					if (item_to_cart.cartItemId)
					{
						cart_promise = cart.updateItem(item_to_cart.cartItemId, item_to_cart).done(function ()
						{
							//If there is an item added into the cart
							if (cart.getLatestAddition())
							{
								if (self.$containerModal)
								{
									self.$containerModal.modal('hide');
								}

								if (layout.currentView instanceof require('Cart.Detailed.View'))
								{
									layout.currentView.showContent();
								}
							}
							else
							{
								self.showError(error_message);
							}
						});
					}
					else
					{
						cart_promise = cart.addItem(item_to_cart).done(function ()
						{
							//If there is an item added into the cart
							if (cart.getLatestAddition())
							{
								if (!isOptimistic)
								{
									layout.showCartConfirmation();
								}
							}
							else
							{
								self.showError(error_message);
							}
						});

						if (isOptimistic)
						{
							cart.optimistic.promise = cart_promise;
							layout.showCartConfirmation();
						}
					}

					// Handle errors. Checks for rollback items.
					cart_promise.fail(function (jqXhr)
					{
						var error_details = null;
						try
						{
							var response = JSON.parse(jqXhr.responseText);
							error_details = response.errorDetails;
						}
						finally
						{
							if (error_details && error_details.status === 'LINE_ROLLBACK')
							{
								var new_line_id = error_details.newLineId;
								self.model.cartItemId = new_line_id;
							}

							self.showError(_('We couldn\'t process your item').translate());

							if (isOptimistic)
							{
								self.showErrorInModal(_('We couldn\'t process your item').translate());
							}
						}
					});

					// Disables the button while it's being saved then enables it back again
					if (e && e.target)
					{
						var $target = jQuery(e.target).attr('disabled', true);

						cart_promise.always(function ()
						{
							$target.attr('disabled', false);
						});
					}
				}
			}



		//@method addToCart add to cart an item, the quantity is written by the user on the input and the options are the same that the ordered item in the previous order
		/*ßaddToCart: function (e)
		{
			e.preventDefault();
			var	self = this
			,	line_id = this.$(e.target).data('line-id')
			,	$form = this.$(e.target).closest('[itemprop="itemListElement"]')
			,	$alert_placeholder = $form.find('[data-type=alert-placeholder]')
			,	$quantity = $form.find('input[name=item_quantity]')
			,	selected_line = this.collection.get(line_id)
			,	item_to_cart = selected_line.get('item')
			,	quantity = isNaN(parseInt($quantity.val(), 10)) ? 0 : parseInt($quantity.val(), 10);

			if (quantity)
			{
				item_to_cart.set('quantity', quantity);

				LiveOrderModel.getInstance().addItem(item_to_cart).done(function ()
				{
					self.trackEventReorder(item_to_cart);

					$alert_placeholder.show().empty();

					var message;

					if (quantity > 1)
					{
						message = _('$(0) Items successfully added to <a href="#" data-touchpoint="viewcart">your cart</a><br/>').translate(quantity);
					}
					else
					{
						message = _('Item successfully added to <a href="#" data-touchpoint="viewcart">your cart</a><br/>').translate();
					}

					message = new CartConfirmationView({
							message: message
						,	type: 'success'
						,	closable: true
					}).render().$el;

					$alert_placeholder.html(message);

					setTimeout(function ()
					{
						$alert_placeholder.fadeOut(function ()
						{
							$alert_placeholder.empty();
						});
					}, 6000);

				}).fail(function (jqXhr)
				{
					jqXhr.preventDefault = true;

					var message = new GlobalViewsMessageView({
						message: ErrorManagement.parseErrorMessage(jqXhr, self.application.getLayout().errorMessageKeys)
					,	type: 'error'
					,	closable: true
					}).render().$el;

					$alert_placeholder.show().empty().html(message);
				});
			}
			else
			{
				var message = new GlobalViewsMessageView({
						message: _('The number of items must be positive.').translate()
					,	type: 'error'
					,	closable: true
				}).render().$el;

				$alert_placeholder.show().empty().html(message);
			}
		}*/



	});

});

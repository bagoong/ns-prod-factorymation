/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       17 Aug 2016     pmas
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */

var TOP_LEVEL_TOPIC = nlapiGetContext().getSetting('SCRIPT', 'custscript_ps_top_level_topic_ids');
var NEWSROOM_TOPIC = nlapiGetContext().getSetting('SCRIPT', 'custscript_ps_newsroom_topic_ids');

function suitelet_knowledgeBase(request, response){
	try {
		var	arrResponse = []
			,	strPage = request.getParameter('page')
			,	strID = request.getParameter('id');

		nlapiLogExecution('debug', 'strPage', strPage);

		switch(strPage) {
			case 'knowledgebase' :
				arrResponse = getTopics();
				break;

			case 'topic':
				arrResponse = getSolutionList(strID);
				break;

			case 'solution':
				arrResponse = getSolutionDetails(strID);
				break;

			case 'newsroom':
				arrResponse = getSolutionList(NEWSROOM_TOPIC, true);
				break;

			case 'newsroomdetails':
				arrResponse = getSolutionDetails(strID, true);
				break;
		}

		response.setContentType("JAVASCRIPT");
		response.write(JSON.stringify(arrResponse));

	} catch(ex) {
		var strErrorMsg = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() + '\n' : ex.toString();
		nlapiLogExecution('Debug', 'SL - Knowledge Base', 'Error encountered: ' + strErrorMsg);
		response.write(JSON.stringify(strErrorMsg));
	}
}

function getTopics(ids) {
	var arrFilters = [], arrColumns = [], arrTopics = [];
	arrFilters.push(new nlobjSearchFilter('isinactive', '', 'is', 'F'));
	if (ids) {
		arrFilters.push(new nlobjSearchFilter('internalid', '', 'anyof', ids.split(',')));
	}

	arrColumns.push(new nlobjSearchColumn('name'));
	arrColumns.push(new nlobjSearchColumn('description'));
	var result = nlapiSearchRecord('topic', 'customsearch1782', arrFilters, arrColumns);

	if (result) {
		for (var i = 0; i < result.length; i++) {
			arrTopics.push({
				id: result[i].getId(),
				name: result[i].getValue('name'),
				description: result[i].getValue('description')
			});
		}
	}

	//nlapiLogExecution('DEBUG', 'topics', JSON.stringify(arrTopics));
	return (arrTopics);
}

function getTopicDetails(recordID) {
	var rec = nlapiLoadRecord('topic', recordID)
		,	objTopic = {};

	objTopic.title = rec.getFieldValue('title');
	objTopic.id = rec.id;

	if (rec.getLineItemCount('subtopic')) {
		objTopic.issubtopic = true;
		objTopic.subtopics = [];
	}

	for (var i = 1; i <= rec.getLineItemCount('subtopic'); i++) {
		objTopic.subtopics.push({
			id: rec.getLineItemValue('subtopic', 'subtopicid', i),
			name: rec.getLineItemValue('subtopic', 'subname', i),
			description: rec.getLineItemValue('subtopic', 'sdescr', i)
		})
	}

	return (objTopic);
}

function getSolutionList(recordID, isnewsroom) {
	var arrFilters = [], arrColumns = [], arrSolutions = [], objResponse = {};
	arrFilters.push(new nlobjSearchFilter('isinactive', '', 'is', 'F'));
	arrFilters.push(new nlobjSearchFilter('internalid', 'topic', 'is', recordID));
	arrFilters.push(new nlobjSearchFilter('status', '', 'is', 'APPROVED'));
	arrFilters.push(new nlobjSearchFilter('isonline', '', 'is', 'T'));

	if (isnewsroom) {
		arrFilters.push(new nlobjSearchFilter('custevent_is_newsroom', '', 'is', 'T'));
		arrColumns.push(new nlobjSearchColumn('internalid').setSort());
	} else {
		arrFilters.push(new nlobjSearchFilter('custevent_is_newsroom', '', 'is', 'F'));
	}

	arrColumns.push(new nlobjSearchColumn('solutioncode'));
	arrColumns.push(new nlobjSearchColumn('title'));
	arrColumns.push(new nlobjSearchColumn('message'));
	arrColumns.push(new nlobjSearchColumn('custevent7'));
	arrColumns.push(new nlobjSearchColumn('custevent8'));
	arrColumns.push(new nlobjSearchColumn('custevent9'));
	arrColumns.push(new nlobjSearchColumn('custevent10'));
	arrColumns.push(new nlobjSearchColumn('custevent11'));
	arrColumns.push(new nlobjSearchColumn('custevent12'));
	arrColumns.push(new nlobjSearchColumn('custevent13'));
	arrColumns.push(new nlobjSearchColumn('custevent14'));
	arrColumns.push(new nlobjSearchColumn('custevent15'));

	var result = nlapiSearchRecord('solution', null, arrFilters, arrColumns);

	// get parent topic record
	var rec = nlapiLoadRecord('topic', recordID);

	if (result) {

		for (var i = 0; i < result.length; i++) {
			arrSolutions.push({
				id: result[i].getId(),
				code: result[i].getValue('solutioncode'),
				title: result[i].getValue('title'),
				message: result[i].getValue('message'),
				links: [
					{
						linkenable: result[i].getValue('custevent7'),
						linkimage: result[i].getValue('custevent7') == "T" ? result[i].getValue('custevent8') : "" ,
						link: result[i].getValue('custevent7') == "T" ? result[i].getValue('custevent9') : ""
					}
					,	{
						linkenable: result[i].getValue('custevent10'),
						linkimage: result[i].getValue('custevent10') == "T" ? result[i].getValue('custevent11') : "" ,
						link: result[i].getValue('custevent10') == "T" ? result[i].getValue('custevent12') : ""
					}
					,	{
						linkenable: result[i].getValue('custevent13'),
						linkimage: result[i].getValue('custevent13') == "T" ? result[i].getValue('custevent14') : "" ,
						link: result[i].getValue('custevent13') == "T" ? result[i].getValue('custevent15') : ""
					}
				]
			})
		}
	} else {
		arrSolutions = getTopicDetails(recordID);
	}

	var arrCategoryTree = [];
	if (isnewsroom) {
		arrCategoryTree = arrSolutions;
	} else {
		arrCategoryTree = getCategoryTree(rec, rec.getFieldValue('parenttopic'));
	}

	objResponse = {
		list: arrSolutions,
		topic: {
			id: rec.id,
			name: rec.getFieldValue('title'),
			parentid: rec.getFieldValue('parenttopic'),
			parentname: rec.getFieldText('parenttopic')
		},
		categories: arrCategoryTree
	};

	//nlapiLogExecution('DEBUG', 'objResponse', JSON.stringify(objResponse));
	return (objResponse);
}

function getSolutionDetails(recordID, isnewsroom) {
	var rec = nlapiLoadRecord('solution', recordID)
		,	arrSolutions = [], objResponse = {};

	if (isnewsroom && rec.getFieldValue('custevent_is_newsroom') != 'T') {
		return (false);
	} else if (!isnewsroom && rec.getFieldValue('custevent_is_newsroom') != 'F') {
		return (false);
	}

	if (rec) {
		var strTopicName = "", strTopicID = "", strParentID = "", strParentName = "", topicRec = "";
		for (var i = 1; i <= rec.getLineItemCount('topics'); i++) {
			strTopicID = rec.getLineItemValue('topics', 'topic', i);
			strTopicName = rec.getLineItemText('topics', 'topic', i);
			break;
		}

		// get topic record
		if (strTopicID && strTopicName) {
			topicRec = nlapiLoadRecord('topic', strTopicID)	// load record since 'parenttopic' is not searchable
			strParentID = topicRec.getFieldValue('parenttopic');
			strParentName = topicRec.getFieldText('parenttopic');
		}

		arrSolutions.push({
			id: rec.id,
			code: rec.getFieldValue('solutioncode'),
			title: rec.getFieldValue('title'),
			message: rec.getFieldValue('message'),
			links: [
				{
					linkenable: rec.getFieldValue('custevent7'),
					linkimage: rec.getFieldValue('custevent7') == "T" ? rec.getFieldValue('custevent8') : "" ,
					link: rec.getFieldValue('custevent7') == "T" ? rec.getFieldValue('custevent9') : ""
				}
				,	{
					linkenable: rec.getFieldValue('custevent10'),
					linkimage: rec.getFieldValue('custevent10') == "T" ? rec.getFieldValue('custevent11') : "" ,
					link: rec.getFieldValue('custevent10') == "T" ? rec.getFieldValue('custevent12') : ""
				}
				,	{
					linkenable: rec.getFieldValue('custevent13'),
					linkimage: rec.getFieldValue('custevent13') == "T" ? rec.getFieldValue('custevent14') : "" ,
					link: rec.getFieldValue('custevent13') == "T" ? rec.getFieldValue('custevent15') : ""
				}
			],
			description: rec.getFieldValue('longdescription')
		});

		var arrCategoryTree = [];
		if (isnewsroom) {
			arrCategoryTree = getSolutionList(NEWSROOM_TOPIC, true).categories;
		} else {
			arrCategoryTree = topicRec ? getCategoryTree(topicRec, strParentID) : ""
		}

		objResponse = {
			solution: arrSolutions,
			topic: {
				id: strTopicID,
				name: strTopicName,
				parentid: strParentID,
				parentname: strParentName
			},
			categories: arrCategoryTree
		};
	}

	//nlapiLogExecution('DEBUG', 'solutions', JSON.stringify(arrSolutions));
	return (objResponse);
}

function getCategoryTree(rec, parentid) {
	var arrTree = []
		,	arrTopics = getTopics()
		,	arrSubTopics = [];

	if (parentid) {
		rec = nlapiLoadRecord('topic', parentid); // override rec if parent is available
	}

	if (rec) {
		for (var i = 1; i <= rec.getLineItemCount('subtopic'); i++) {
			arrSubTopics.push({
				id: rec.getLineItemValue('subtopic', 'subtopicid', i),
				name: rec.getLineItemValue('subtopic', 'subname', i),
				isactive: rec.id == rec.getLineItemValue('subtopic', 'subtopicid', i) ? true : false
			});
		}
	}

	for (var i = 0; i < arrTopics.length; i++) {
		arrTree.push({
			id: arrTopics[i].id,
			name: arrTopics[i].name,
			subtopics: arrTopics[i].id == rec.id ? arrSubTopics : [],
			isactive: rec.id == arrTopics[i].id ? true : false
		});
	}

	return (arrTree);
}
